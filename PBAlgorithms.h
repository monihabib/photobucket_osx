//
//  PBAlgorithms.h
//  SameImageCutter
//
//  Created by Mostafizur Rahman on 10/18/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBFileProperties.h"
@interface PBAlgorithms : NSObject
+(int)getSearchIndex:(NSMutableArray *)arrayToUpdate withData:(long)fileDataLength;
@end
