//
//  CollectionViewItem.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "CollectionViewItem.h"

@interface CollectionViewItem (){
    
    
}
@end

@implementation CollectionViewItem


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.wantsLayer = YES;
    self.view.layer.borderColor = [THEME_COLOR CGColor];
    
}

-(void)viewDidDisappear{
    [super viewDidDisappear];
    self.selectionDelegate = nil;
    self.itemImageView.image = nil;
    
}


-(void)mouseUp:(NSEvent *)event{
    [self setHighlightState:NSCollectionViewItemHighlightNone];
    [_selectionDelegate onItemSelected:_selectionIndexPath onButton:NO isSelection:NO];
}
-(void)mouseDown:(NSEvent *)event{
    [super mouseDown:event];
    [self setHighlightState:NSCollectionViewItemHighlightForSelection];
}
-(void)setHighlightState:(NSCollectionViewItemHighlightState)highlightState{
    self.view.layer.borderWidth = highlightState == NSCollectionViewItemHighlightForSelection ? 2 : 0;
}

- (IBAction)selectItemCell:(id)sender {
    
    _isSelected = !_isSelected;
    [PBUserInterfaceProvider setCheckedImage:sender isChecked:_isSelected];
    [_selectionDelegate onItemSelected:_selectionIndexPath onButton:YES isSelection:_isSelected];
}
@end
