//
//  PBButtonView.m
//  PhotoBucket
//
//  Created by Mostafizur Rahman on 10/27/16.
//  Copyright © 2016 Knowstack. All rights reserved.
//

#import "PBButtonView.h"

@interface PBButtonView (){
    
    NSTrackingArea *trackingArea;
    NSImageView *buttonLogoImageView;
    NSTextField *buttonNameLabel;
    BOOL isMouseExited;
    BOOL isMouseDown;
}

@end

@implementation PBButtonView

-(id)init {
    self = [super init];
    [self updateTrackingAreas];
    _isSelected = NO;
    return self;
}


-(BOOL)becomeFirstResponder {
    
    return YES;
}

-(void)mouseUp:(NSEvent *)event {
    
    if(!isMouseExited) {
        [PBUserInterfaceProvider setMouseHoverLook:self];
    }
    [PBUserInterfaceProvider setDefaultImageView:buttonLogoImageView withTitleLabel:buttonNameLabel];
    _isSelected = isMouseDown && !isMouseExited;
    
    if (_isSelected) {
        [_selectionDelegate onSelectionCompleted:self];
    }
    isMouseDown = NO;
}

-(void)mouseDown:(NSEvent *)event {
    
    isMouseDown = YES;
    [PBUserInterfaceProvider setMouseClickedLook:self];
    [PBUserInterfaceProvider setHighlightedImageView:buttonLogoImageView withTitleLabel:buttonNameLabel];
}

-(void)mouseEntered:(NSEvent *)event {
    
    isMouseExited = NO;
    [PBUserInterfaceProvider setMouseHoverLook:self];
}

-(void)mouseExited:(NSEvent *)event {
    
    isMouseExited = YES;
    [PBUserInterfaceProvider setMouseExitLook:self];
}

- (void)drawRect:(NSRect)dirtyRect {
    
    [super drawRect:dirtyRect];
    buttonLogoImageView = [self viewWithTag:TAG_BUTTONLOGO];
    buttonNameLabel = [self viewWithTag:TAG_LABELTITLE];
}


-(void)updateTrackingAreas {
    
    if(trackingArea != nil) {
        [self removeTrackingArea:trackingArea];
    }
    
    int opts = (NSTrackingMouseEnteredAndExited | NSTrackingActiveAlways);
    trackingArea = [ [NSTrackingArea alloc] initWithRect:[self bounds]
                                                 options:opts
                                                   owner:self
                                                userInfo:nil];
    [self addTrackingArea:trackingArea];
}

-(void)setActionTagValue:(NSInteger)actionTagValue {
    
    _actionTagValue = actionTagValue;
}
@end
