//
//  PBInfoAlert.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/26/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBInfoAlert.h"

@implementation PBInfoAlert

-(id)initWithType:(AlertTypes) alertType withOptions:(NSDictionary *)options{
    
    self = [super init];
    
    if(alertType == ALERTTYPE_EMPTYDIRECTORY ) [self setupAlertForEmptyDestDir:options];
    else if(alertType == ALERTTYPE_HIDEDETAILSVIEW) [self setupAlertForDetailsViewON:options];
    else if(alertType == ALERTTYPE_WAITUNTIFINISH) [self setupAlertWaitUntilFinish];
    else if(alertType == ALERTTYPE_MEMORYOVERFLOW);
    else if(alertType == ALERTTYPE_INVALIDDIRECTORY) [self setupAlertForInvalidDir:options];
    else if(alertType == ALERTTYPE_RELOADCOLLECTION) [self setupAlertForReloadCollection];
    return self;
}

-(void)setupAlertForEmptyDestDir:(NSDictionary *)options {
    [self setAccessories:@"empty_dir" accessoryImage:@"broken_link"];
    NSString *title = options.allKeys.firstObject;
    NSString *message = [options objectForKey:title];
    self.informativeText = message;
    self.messageText = title;
}

-(void)setupAlertForDetailsViewON:(NSDictionary *)options {
    [self setAccessories:@"warning" accessoryImage:@"top"];
    self.informativeText = @"To perform this operation hide preview. Click top right corner arrow button.";
    self.messageText = @"Hide Details View!";
}

-(void)setupAlertForInvalidDir:(NSDictionary *)options {
    [self setAccessories:@"invalid_icon" accessoryImage:@"broken_link"];
    NSString *pathString = [options objectForKey:@"root_path"];
    self.informativeText = [NSString stringWithFormat:@"Root Directory path \"%@\" is invalid. Please! Select valid root directory.", pathString];
    self.messageText = @"Invalid Directory Path";
}

-(void)setupAlertWaitUntilFinish {
    [self setAccessories:@"invalid_icon" accessoryImage:@"hourglass"];
    self.informativeText = @"Processor is little busy! Please wait until finish.";
    self.messageText = @"Operation in progress!!!";
}


-(void)setupAlertForReloadCollection {
    [self setAccessories:@"invalid_icon" accessoryImage:@"previewred"];
    [self addButtonWithTitle:@"No"];
    self.informativeText = @"Images have been loaded from root directory path. Do you want to reload it?";
    self.messageText = @"Reload Images";
    
}



-(void)setAccessories:(NSString *)iconName accessoryImage:(NSString *)accImageName{
    self.icon = [NSImage imageNamed:iconName];
    NSView *accessoryView = [[NSView alloc] initWithFrame:CGRectMake(0, 0, 275, 100)];
    accessoryView.wantsLayer = YES;
    accessoryView.layer.contents = [NSImage imageNamed:accImageName];
    accessoryView.layer.contentsGravity = kCAGravityResizeAspect;
    self.alertStyle = NSInformationalAlertStyle;
    self.accessoryView = accessoryView;
    [self addButtonWithTitle:@"Okay"];
}
@end
