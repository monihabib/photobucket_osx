//
//  PhotoCollectionView.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 12/13/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PBDefines.h"
#import "PBFileProperties.h"
#import "CollectionViewItem.h"
#import "PBSectionHeaderView.h"
#import "PBAppDelegate.h"
#import "PBDirectoryExtractor.h"
#import "PBUserInterfaceProvider.h"


@protocol PBCollectionItemSelectionDelegate <NSObject>
-(void)onItemSelected:(PBFileProperties *)fileProperties;
@end

@interface PhotoCollectionView : NSCollectionView<
NSCollectionViewDelegate,
NSCollectionViewDataSource,
NSCollectionViewDelegateFlowLayout,
PBItemSelectionDelegate,
PBSearchingDelegate>

@property (readwrite) PBDirectoryExtractor *directoryExtractor;

@property (readwrite, weak) NSTextField *rootPathTextField;
@property (readwrite, weak) id<PBCollectionItemSelectionDelegate> selectionDelegate;
//@property (readwrite) SearchTypes searchTypeIndexed;
@property (readwrite) BOOL isListing;
@property (readwrite) BOOL isDetailsViewActive;
@property (readwrite) BOOL isSelectAll;
@property (readonly) NSComboBox *groupingOptionComboBox;
-(BOOL)isConfiguredSearchingWithOptions:(NSDictionary *)searchingOptions;
-(void)didFoundSearchedItem:(NSArray *)searchedFoundArray;
-(void)refreshComboBox ;
@end
