//
//  PBDefines.h
//  PhotoBucket
//
//  Created by Mostafizur Rahman on 10/29/16.
//  Copyright © 2016 Knowstack. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define  THEME_COLOR [NSColor colorWithRed:148./225. green:0 blue:148./225. alpha:1]

#pragma -mark ACTION TAGS
#define ACITON_FILEMANAGER 6135
#define ACTION_PREVIEWIMAGE 2073
#define ACTION_SEND_IMAGE 8879
#define ACTION_RECEIVE_IMAGE 3265
#define ACTION_FILESHARING 3129
#define ACTION_SEARCH_IMAGE 3721
#define MAX_THUMB_DIMENSION 150

#pragma -mark TAG INTERFACE

#define RANDOMSTRING_LENGTH 6

#define BORDER_WIDTH 1.4
#define MAX_COLOR_VALUE 255.

#define COLOR_INC 25.
#define START_COLOR 150.
#define TAG_STATTEXT  222
#define TAG_BUTTONLOGO 2222
#define TAG_LABELTITLE 1111
#define TAG_ABOUTUS 1138
#define TAG_DIRECTORY 1139
#define TAG_TEXTFIELD 1137
#define TAG_SELECTALL 3232
#define TAG_DELETEALL 3131

#define NOTICE_IMAGE_TAG 758
#define TAG_URLTITLE 757
#define TAG_TITLEHEADER 1144
//#define NOTICE_TITLE_URLNAME 1155
#define TAG_NOTICETITLE 756
#define TAG_IMAGECOUNT 755

#define EMPTY_STRING @""
#define NIBNAME_STATUS @"PBStatusView"
#define NIBNAME_NOTICE @"PBNoticeView"
#define NIBNAME_DETAIL @"PBDetailsView"
#define NIBNAME_MANAGE @"PBFileManagerView"
#define NIBNAME_HEADER @"PBSectionHeaderView"
#define DELETETITLE @"Delete"
#define DELETEMSG @"Deleted Path :"
#define REDUNDENTTITLE @"redundant"
#define REDUNENTMSG @"Redundant Path :"

#define PBVIEW_CONTROLLER @"PBViewController"
#define LAYER_BORDERLINE @"BoarderLine"
#define CONTENT_NIB_NAME_PREVIE_IMAGE @"PBPhotoCollectionViewController"
#define LOGO_RED @"red"
#define LOGO_BLACK @"black"
#define FORMAT_DSTR @"%@%@"
#define FORMAT_MMDD @"%@ %@"
#define FORMAT_GPS @"%@ %@ / %@ %@"
#define FORMAT_CAMMODEL @"%@, %@"
#define FORMAT_TOTALIMAGE @"total images %ld"
#define FORMAT_DUPLICATEIMAGE @"duplicate images %ld"
#define FORMAT_FILESIZE @"%ld%@"
#define FORMAT_COUNT @"%ld"
#define FORMAT_DATESTR @"%ld %@, %ld"
#define FORMAT_FOLDER @"%ld %@, %@"
#define FORMAT_LENPREDICATE @"self.fileDataLength == %@"
#define FORMAT_DUPFILENAME @"%@_%@.%@"
#define FORMAT_LONG @"%ld"
#define FORMAT_URLSPACE @"%20"
#define FORMAT_UTFSPACE @" "
#define FORMAT_COLON @":"
#define FORMAT_CHAR @"%C"
#define FORMAT_RESOLUTION @"%ldx%ld"
#define FORMAT_SIZE @"Size : %@"
#define FORMAT_ARG @"%@"

#define IMAGE_TYPE [NSArray arrayWithObjects:  @"png", @"jpg",@"jpeg",@"gif", nil]
#define DATE_COMPONENTS NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute
#pragma -mark FILE PROPERTIES KEY

#define FOLDERNAME_IMGBKT @"___Image_Bucket____"
#define FOLDERNAME_THUMB @"___thumbnailImageFolder___"
#define IMAGE_NAME_THUMB @"___thumbnailImageFolder___/%@.jpg"

#define UNKNOWN_MODEL @"Unknown Model"
#define NOCOMMENT @"no comments!"
#define NOGPSDATA @"No GPS Data!"
#define KEY_EXTATTRIBUTE @"NSFileExtendedAttributes"
#define KEY_DATEMODIFY @"NSFileModificationDate"
#define KEY_FINDERCOMMENT @"com.apple.metadata:kMDItemFinderComment"
#define KEY_DATECREATION @"NSFileCreationDate"
#define KEY_FILENUMBER @"NSFileSystemFileNumber"
#define KEY_UNDERROR @"NSUnderlyingError"

#define IDENTIFIER_ITEM @"Item"
#define IDENTIFIER_HEADER_SEARCH @"SearchHeaderView"
#define IDENTIFIER_HEADER_SECTION @"HeaderView"
#define FORMATTER_FILESIZE @"%dKB"
#define FORMATTER_RESOLUTION @"%dX%d"
#define TITLE_MOVING @"Moving images..."
#define TITLE_LOADING @"Loading Photos..."
#define TITLE_STATUS @"Listing Image..."
#define TITLE_DELETE @"Deleting redundant images..."
#define TITLE_EMPTY @"Empty Root Directory"
#define TITLE_ROOTURL @"Root Url"
#define TITLE_FREESPACE @"Freed Space"
#define TITLE_IMGCOUNT @"Total No. of Images"
#define TITLE_DELSTAT @"Deletion Statistics"
#define TITLE_DELMSG @"Total data deleted %@"
#define FONT_HELVATICA @"Helvetica Neue"


#define Message "Photo bucket does not store any information regarding any image or its properties such as Image taken date, file size, Underlying comments, Location etc. It is strictly illegal to distribute any copy of Photo Bucket without App Store. Photo bucket and its employees shall not be held liable for any deletion operation performed."


#define ARRAY_MONTHNAME [NSArray arrayWithObjects: @"January", @"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December",nil]
#define ARRAY_SIZESTRING [NSArray arrayWithObjects:@" KB", @" MB", @" GB", nil]
#define SEARCH_PREDICATE @"self.imageCameraModel contains[cd] %@ || self.fileType contains[cd] %@ || self.fileName contains[cd] %@ || self.imageHashTag contains[cd] %@"
#define NUMERIC_ALPHABET @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

#define CONST_SPACING 20
#define CONST_EDGE 30
#define CODE_OKAY 1000

#define ANIMATION_DURATION 0.4f

#pragma -mark ANIMATION KEY AND VALUES

#define ANIMATIO_KEYPATH_OPACITY  @"opacity"
#define ANIMATIO_KEYPATH_FADEAWAY @"FadeAnimation"
#define KEY_DIRECTORY @"root_path"
#define KEY_SEARCHINDEX @"TypeIndex"
#define KEY_SINGLEINSTANCE @"single_instances"
#define KEY_TEXTMOVEMENT @"NSTextMovement"
#define CONST_100 100
#define CONST_HALF 0.5
#define MAX_ANIM_DURATION 2
#define MAX_ANIM_WIDTH 250
#define MAX_ANIM_INSTANCE 25
#define HEIGHT_STATUSVIEW 85
#define MAX_TRANSFORM_ANGLE M_PI * 2.0 / MAX_ANIM_INSTANCE
#define ERRORCODE_FILEEXIST 17
#define SIZE_ONEK 1000

typedef NS_ENUM(NSUInteger, MOVE_TYPE)  {
    MOVETYPE_FOLDERHIGHERARCHI = 1,
    MOVETYPE_CREATIONTIME = 0,
    MOVETYPE_USERCOMMENTS = 2
};

#define CATEGORY_COUNT 4
#define MINIMUM_COUNT 1
#define MINIMUM_SPACING 20
#define ZERO 0

enum ALERT_TYPE {
    ALERTTYPE_EMPTYDIRECTORY,
    ALERTTYPE_WAITUNTIFINISH,
    ALERTTYPE_MEMORYOVERFLOW,
    ALERTTYPE_HIDEDETAILSVIEW,
    ALERTTYPE_INVALIDDIRECTORY,
    ALERTTYPE_INVALIDPATH,
    ALERTTYPE_RELOADCOLLECTION,
};
typedef enum ALERT_TYPE AlertTypes;


enum SEARCH_GROUP_TYPE {
    
    GROUPBY_CAPTUREDATE,
    GROUPBY_FOLDERNAME,
    GROUPBY_FILETYPE,
    GROUPBY_COMMENTS,
    GROUPBY_DUPLICATEIMAGE,
    GROUPBY_NONE,
    GROUPBY_INACTIVE,
    GROUPBY_ACTIVE,
};
typedef enum SEARCH_GROUP_TYPE SearchTypes;


@interface PBDefines : NSObject 
+(NSString *)getMonthName:(NSUInteger)monthNumber;
@end
