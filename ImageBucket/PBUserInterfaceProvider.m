//
//  PBUserInterfaceProvider.m
//  PhotoBucket
//
//  Created by Mostafizur Rahman on 10/27/16.
//  Copyright © 2016 Knowstack. All rights reserved.
//

#import "PBUserInterfaceProvider.h"


@implementation PBUserInterfaceProvider

+(CALayer *)getBorderLayerWithLength:(const CGFloat)borderLength {
    
    CALayer *layerdBorder = [CALayer layer];
    layerdBorder.borderColor = [NSColor darkGrayColor].CGColor;
    layerdBorder.borderWidth = BORDER_WIDTH;
    layerdBorder.frame = CGRectMake(-1, -1, borderLength, BORDER_WIDTH);
    layerdBorder.name = LAYER_BORDERLINE;
    return layerdBorder;
}


+(CALayer *)getBorderLayer:(const CGFloat)borderLength {
    
    CALayer *layerdBorder = [CALayer layer];
    layerdBorder.borderColor = THEME_COLOR.CGColor;
    layerdBorder.borderWidth = BORDER_WIDTH;
    layerdBorder.frame = CGRectMake(-1, -1, borderLength, BORDER_WIDTH);
    layerdBorder.name = LAYER_BORDERLINE;
    return layerdBorder;
}

+(void)setMouseHoverLook:(NSView *)inputView
{
    NSArray *layers = inputView.layer.sublayers;
    for (CALayer *layer in layers) {
        if ([layer.name isEqualToString:LAYER_BORDERLINE]) {
            layer.borderColor = [THEME_COLOR CGColor];
            break;
        }
    }
    inputView.wantsLayer = YES;
    inputView.layer.backgroundColor = [NSColor colorWithRed:.3 green:.1 blue:.1 alpha:.5].CGColor;
}

+(void)setMouseClickedLook:(NSView *)inputView {
    
    NSArray *layers = inputView.layer.sublayers;
    
    for (CALayer *layer in layers) {
        if ([layer.name isEqualToString:LAYER_BORDERLINE]) {
            layer.borderColor = [NSColor greenColor].CGColor;
            break;
        }
    }
    inputView.wantsLayer = YES;
    inputView.layer.backgroundColor = [NSColor lightGrayColor].CGColor;
}


+(void)setMouseExitLook:(NSView *)inputView
{
    NSArray *layers = inputView.layer.sublayers;
    for (CALayer *layer in layers) {
        if ([layer.name isEqualToString:LAYER_BORDERLINE]) {
            layer.borderColor = [NSColor darkGrayColor].CGColor;
            break;
        }
    }
    inputView.wantsLayer = YES;
    inputView.layer.backgroundColor = [NSColor clearColor].CGColor;
}

+(void)setHighlightedImageView:(NSImageView *)logoImageView
                withTitleLabel:(NSTextField *)titleTextField
{
    [PBUserInterfaceProvider alternateImageViewName:logoImageView withCurrentName:LOGO_BLACK byName:LOGO_RED];
    [titleTextField setTextColor:THEME_COLOR];
}
+(void)setDefaultImageView:(NSImageView *)logoImageView
            withTitleLabel:(NSTextField *)titleTextField
{
    [PBUserInterfaceProvider alternateImageViewName:logoImageView withCurrentName:LOGO_RED byName:LOGO_BLACK];
    [titleTextField setTextColor:[NSColor blackColor]];
}

+(void)alternateImageViewName:(NSImageView *)imageView withCurrentName:(NSString *)curName byName:(NSString *)repName
{
    NSString *imageName = [imageView identifier];
    NSString *highlightedLogoName = [[imageName componentsSeparatedByString:curName] firstObject];
    highlightedLogoName = [NSString stringWithFormat:FORMAT_DSTR, highlightedLogoName,repName];
    imageView.image = [NSImage imageNamed:highlightedLogoName];
    imageView.identifier = highlightedLogoName;
}

+(void)setActivityIndicator:(NSString *)titleString
               inParentView:(NSView *)_contentViewContainer
                     toView:(NSView *)baseIndicatorView{
    
    baseIndicatorView.alphaValue = CONST_HALF;
    baseIndicatorView.wantsLayer = YES;
    
    const CGFloat midX = CGRectGetMidX(_contentViewContainer.bounds);
    const CGFloat midY = CGRectGetMidY(_contentViewContainer.bounds);
    
    
    baseIndicatorView.frame = CGRectMake(midX - MAX_ANIM_WIDTH * CONST_HALF,
                                         midY - MAX_ANIM_WIDTH * CONST_HALF,
                                         MAX_ANIM_WIDTH, MAX_ANIM_WIDTH);;
    CAReplicatorLayer* replicatorLayer = [CAReplicatorLayer new];
    replicatorLayer.name = @"LoadingAnimationLayer";
    replicatorLayer.frame  = CGRectMake(0, 0,
                                        MAX_ANIM_WIDTH, MAX_ANIM_WIDTH);
    replicatorLayer.instanceCount = MAX_ANIM_INSTANCE;
    replicatorLayer.instanceDelay = 1.0 / MAX_ANIM_INSTANCE;
    replicatorLayer.preservesDepth = false;
    replicatorLayer.instanceColor = [NSColor whiteColor].CGColor;
    
    // 3
    replicatorLayer.instanceRedOffset   = 0.0;
    replicatorLayer.instanceGreenOffset = -CONST_HALF;
    replicatorLayer.instanceBlueOffset  = -CONST_HALF;
    replicatorLayer.instanceAlphaOffset = 0.0;
    
    // 4
    const CGFloat angle = MAX_TRANSFORM_ANGLE;
    replicatorLayer.instanceTransform = CATransform3DMakeRotation(angle, 0.0, 0.0, 1.0);
    [baseIndicatorView.layer addSublayer: replicatorLayer];
    
    CALayer *instanceLayer = [CALayer new];
    CGFloat layerWidth = MAX_ANIM_WIDTH / 8;
    instanceLayer.frame = CGRectMake(MAX_ANIM_WIDTH / 2, 0, layerWidth / 3, layerWidth);
    instanceLayer.backgroundColor = [NSColor whiteColor ].CGColor;
    [replicatorLayer addSublayer : instanceLayer];
    
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:ANIMATIO_KEYPATH_OPACITY];
    fadeAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    fadeAnimation.toValue   = [NSNumber numberWithFloat:0.0];
    fadeAnimation.duration  = MAX_ANIM_DURATION;
    fadeAnimation.repeatCount = INFINITY;
    
    instanceLayer.opacity = 0.0;
    [instanceLayer addAnimation:fadeAnimation forKey:ANIMATIO_KEYPATH_FADEAWAY];
    [baseIndicatorView.layer addSublayer:replicatorLayer];
    
    NSTextField *textField = [PBUserInterfaceProvider getTextFiledWith:CGRectMake(MAX_ANIM_WIDTH/2 - 100 , MAX_ANIM_WIDTH/2 - 10, 200, 20)
                                                              forTitle:titleString
                                                            alignement:NSTextAlignmentCenter
                                                              tagValeu:7132];
    
    [baseIndicatorView addSubview:textField];
    [_contentViewContainer addSubview:baseIndicatorView];
    
}



+(NSTextField *)getTextFiledWith:(CGRect)textFieldFrame
                        forTitle:(NSString *)titleString
                      alignement:(NSTextAlignment)textAlignment
                        tagValeu:(NSUInteger)tag {
    
    NSTextField *textField = [[NSTextField alloc] initWithFrame:textFieldFrame];
    [textField setBezeled:NO];
    [textField setDrawsBackground:NO];
    [textField setEditable:NO];
    [textField setSelectable:NO];
    textField.tag = tag;
    textField.stringValue = titleString;
    [textField setAlignment:textAlignment];
    return textField;
}

+ (NSView *)loadWithNibNamed:(NSString *)nibNamed owner:(id)owner class:(Class)loadClass {
    NSNib *nib = [[NSNib alloc] initWithNibNamed:nibNamed bundle:nil];
    NSArray *objects;
    if (![nib instantiateWithOwner:owner topLevelObjects:&objects]) return nil;
    for (id object in objects) {
        if ([object isKindOfClass:loadClass]) {
            return object;
        }
    }
    return nil;
}

+(void)configureActionInViewArray:(NSMutableArray *)buttonArray
                     withDelegate:(id)callBackDelegate {
    
    NSInteger tagArray[] = {ACTION_FILESHARING, ACITON_FILEMANAGER,  ACTION_PREVIEWIMAGE};
    for(int i = 0; i < buttonArray.count; i++) {
        PBButtonView *btnView = [buttonArray objectAtIndex:i];
        btnView.actionTagValue = tagArray[i];
        btnView.selectionDelegate = callBackDelegate;
        [PBUserInterfaceProvider setBottomLineLayerInView:btnView];
    }
}

+(void)setBackgroundColorToArray:(NSMutableArray *)rootPanelArray {    
    for(float i = COLOR_INC; i < CONST_100; i += COLOR_INC) {
        const int index = i / COLOR_INC - 1 ;
        NSView *containerView = [rootPanelArray objectAtIndex: index];
        [containerView setWantsLayer: YES];
        [containerView.layer setBackgroundColor: [[NSColor colorWithRed:(START_COLOR + i ) / MAX_COLOR_VALUE
                                                                  green:(START_COLOR + i ) / MAX_COLOR_VALUE
                                                                   blue:(START_COLOR + i ) / MAX_COLOR_VALUE
                                                                  alpha:1.] CGColor]];
    }
}

+(void)setBottomLineLayerInView:(NSView *)inputView {
    
    CGFloat width = inputView.frame.size.width;
    CALayer *bottomLineLayer  = [PBUserInterfaceProvider getBorderLayerWithLength:width];
    [inputView.layer insertSublayer:bottomLineLayer atIndex:0];
}

+(void)setCheckedImage:(NSButton *)button isChecked:(BOOL)isChecked {
    NSString *imageName = isChecked ? @"checked" : @"unchecked";
    NSImage *image = [NSImage imageNamed:imageName];
    button.image =  image;
}


+(void)setAutoLayoutConstraintXYCenterTopInView:(NSView *)parentView toView:(NSView *)childView {
    [parentView addSubview:childView];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1.0f constant:0.0f]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:childView.frame.size.height]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:childView.frame.size.width]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeCenterX
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:childView.frame.origin.x + childView.frame.size.width / 2]];
    [childView setTranslatesAutoresizingMaskIntoConstraints:NO];
}

+(void)setAutoLayoutConstraintXYCenterBottomInView:(NSView *)parentView toView:(NSView *)childView {
    
    [parentView addSubview:childView];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1.0f constant:childView.frame.origin.y]];
    
    [childView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:childView.frame.size.height]];
    
    [childView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:childView.frame.size.width]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeCenterX
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeCenterX
                                                          multiplier:1.0
                                                            constant:0]];
    [childView setTranslatesAutoresizingMaskIntoConstraints:NO];
}









+(void)setAutolayoutConstraintWithFixedHeightInView:(NSView *)parentView
                                             toView:(NSView *)childView {
    
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:childView.frame.size.height]];
    
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeBottom
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeBottom
                                                          multiplier:1.0f constant:0.0f]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeLeading
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeLeading
                                                          multiplier:1.0f constant:0.0f]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeTrailing
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeTrailing
                                                          multiplier:1.0f constant:0.0f]];
    [childView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
}


+(void)setAutoLayoutConstraintInView:(NSView *)parentView toView:(NSView *)childView {
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeBottom
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeBottom
                                                          multiplier:1.0f constant:0.0f]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeLeading
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeLeading
                                                          multiplier:1.0f constant:0.0f]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeTrailing
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeTrailing
                                                          multiplier:1.0f constant:0.0f]];
    [parentView addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:parentView
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1.0f constant:0.0f]];
    [childView setTranslatesAutoresizingMaskIntoConstraints:NO];
}

+(void)addSubview:(NSView *)subView toParentView:(NSView *)parentViewContainer{
    [parentViewContainer addSubview:subView];
    [PBUserInterfaceProvider setAutoLayoutConstraintInView:parentViewContainer toView:subView];
}

+ (NSString *)getRandomStringOfLen:(NSInteger)length
{
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    const NSUInteger CHAR_LEN = [NUMERIC_ALPHABET length];
    for (int i = 0; i < length; i++) {
        [randomString appendFormat:FORMAT_CHAR, [NUMERIC_ALPHABET characterAtIndex:arc4random() % CHAR_LEN]];
    }
    
    return randomString;
}

+(NSString *)getDataSizeString:(long)fileSize
{
    NSString * sizeString = EMPTY_STRING;
    for (int index = ZERO; fileSize > SIZE_ONEK; index++)
    {
        fileSize = fileSize / SIZE_ONEK;
        sizeString = [NSString stringWithFormat:FORMAT_FILESIZE, fileSize, [ARRAY_SIZESTRING objectAtIndex:index]];
    }
    return sizeString;
}

+(BOOL)moveFileUsing:(NSFileManager *)fileManager destinationUrl:(NSURL *)destinationURL sourceUrl:(NSURL *)sourceFileURL error:(NSError*__autoreleasing*)movingError{
    while([fileManager fileExistsAtPath:destinationURL.path]){
        NSString* extension = destinationURL.pathExtension;
        destinationURL = [destinationURL URLByDeletingPathExtension];
        NSString *fileName = destinationURL.lastPathComponent;
        destinationURL = [destinationURL URLByDeletingLastPathComponent];
        fileName = [NSString stringWithFormat:FORMAT_DUPFILENAME,fileName, [PBUserInterfaceProvider getRandomStringOfLen:RANDOMSTRING_LENGTH], extension];
        destinationURL = [destinationURL URLByAppendingPathComponent:fileName];
    }
    return [fileManager moveItemAtURL:sourceFileURL toURL:destinationURL error:movingError];
}

@end
