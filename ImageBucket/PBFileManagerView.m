//
//  FileManagerViewController.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/23/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBFileManagerView.h"


@interface PBFileManagerView (){
    
    NSURL *destinationURL;
    
    BOOL shouldDeleteDuplicateFiles;
    BOOL isFileManagerBusy;
    MOVE_TYPE moveType;
}

@end

@implementation PBFileManagerView
@synthesize directoryExtractor, parentContainerView;

-(instancetype)init {
    self = [super init];
    isFileManagerBusy = NO;
    return self;
}

-(void)awakeFromNib{
    [self.movingOptionsCombobox selectItemAtIndex:0];
    moveType = MOVETYPE_CREATIONTIME;
    
    isFileManagerBusy = NO;
}


- (IBAction)checkedDuplicateFileDeletionOption:(id)sender {
    shouldDeleteDuplicateFiles = ((NSButton *)sender).state == 1;
}

- (IBAction)moveFileToDestinationDirectory:(id)sender {
    if(destinationURL && [directoryExtractor.imagePropertiesArray count] > 0){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self moveImageFiles];
        });
    } else {
        [self showAlertViewFor:ALERTTYPE_EMPTYDIRECTORY];
    }
    
}

-(void)moveImageFiles{
    if(isFileManagerBusy) [self showAlertViewFor:ALERTTYPE_WAITUNTIFINISH];
    [self setFileManagerBusyStatus];
    if(shouldDeleteDuplicateFiles){
        [directoryExtractor deleteDuplicateFiles];
    }
    if(moveType== MOVETYPE_CREATIONTIME) [directoryExtractor moveFilesAccordingToDateTime:destinationURL];
    else if(moveType== MOVETYPE_USERCOMMENTS) [directoryExtractor moveCommentedFiles:destinationURL];
    else if(moveType== MOVETYPE_FOLDERHIGHERARCHI) [directoryExtractor moveFilesAccordingToFolderHierarchy:destinationURL];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [directoryExtractor.statusView removeFromSuperview];
        [directoryExtractor.fileManagerDelegate onMoveCompleted];
        isFileManagerBusy = NO;
    });
}

- (IBAction)deleteAllDuplicateFile:(id)sender {
    
    if(isFileManagerBusy) [self showAlertViewFor:ALERTTYPE_WAITUNTIFINISH];
    [self setFileManagerBusyStatus];
    [self addStatusView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [directoryExtractor deleteDuplicateFiles];
        dispatch_async(dispatch_get_main_queue(), ^{
            [directoryExtractor.statusView removeFromSuperview];
            [directoryExtractor.fileManagerDelegate onDeletionCompleted];
            isFileManagerBusy = NO;
        });
    });
}

-(void)onDirectorySelected:(NSURL *)destinationUrl{
    destinationURL = [directoryExtractor createFolderIn:destinationUrl withFolderName:FOLDERNAME_IMGBKT thumbnail:NO];
    _destinationDirectoryTextField.stringValue = destinationURL.path;
}

-(void)setFileManagerBusyStatus {
    [directoryExtractor.fileManagerDelegate onFileManagerBusy];
    directoryExtractor.totalImageCount = 0;
    isFileManagerBusy = YES;
}

- (IBAction)movingOptionSelected:(NSComboBox *)sender {
    moveType = sender.indexOfSelectedItem;
}


-(void)addStatusView{
    [parentContainerView addSubview:directoryExtractor.statusView];
    directoryExtractor.statusView.frame = NSMakeRect(0, 0, parentContainerView.frame.size.width, HEIGHT_STATUSVIEW);
    [PBUserInterfaceProvider setAutolayoutConstraintWithFixedHeightInView:parentContainerView toView:directoryExtractor.statusView];
}

-(void)showAlertViewFor:(AlertTypes)alertType{
    
    NSDictionary *options;
    if([directoryExtractor.imagePropertiesArray count ] == 0)
        options = [NSDictionary dictionaryWithObject:@"No image found in \"Root Directory\" folder" forKey:@"Image Not Found!!!"];
    else options = [NSDictionary dictionaryWithObject:@"Folder path to the \"Destination Directory\" is invalid" forKey:@"Destination Directory Invalid!!!"];
    PBInfoAlert *alert = [[PBInfoAlert alloc] initWithType:alertType withOptions:options];
    [alert beginSheetModalForWindow:[[self superview] window] completionHandler:^(NSModalResponse returnCode) {
        
    }];
}


- (IBAction)openDestinationFolderChooser:(id)sender {
    NSOpenPanel *panel = [[NSOpenPanel alloc] init];
    [panel setCanChooseFiles:NO];
    [panel setCanChooseDirectories:YES];
    [panel setAllowsMultipleSelection:NO];
    [panel beginSheetModalForWindow:[self superview].window completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSFileHandlingPanelOKButton) {
            for (NSURL *url in [panel URLs]) {
                self.destinationDirectoryTextField.stringValue = url.path;
                [self onDirectorySelected:url];
                break;
            }
        }
    }];
}



@end
