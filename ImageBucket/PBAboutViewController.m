//
//  PBAboutViewController.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 3/15/17.
//  Copyright © 2017 Mostafizur Rahman. All rights reserved.
//

#import "PBAboutViewController.h"
#import "PBPhotoViewController.h"


@interface PBAboutViewController ()

@end

@implementation PBAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear{
    [super viewWillAppear];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(resizeWindow) name:NSWindowDidResizeNotification object:nil];

    [self resizeWindow];
}

-(void)viewWillDisappear{
    [super viewWillDisappear];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSWindowDidResizeNotification object:nil];
}

- (IBAction)exitViewController:(NSButton *)sender {
    [self dismissController:self];
}

-(void)resizeWindow {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CGRect frame = self.presentingViewController.view.frame;
        [self.view.window setFrame:frame display:YES animate:YES];
    });
}

- (IBAction)openFaceBookPage:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString: @"https://www.facebook.com/imagebucket.hashtag/"]];
    
}
- (IBAction)openTwitterPage:(id)sender {
     [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString: @"https://twitter.com/photobuckets/"]];
}
- (IBAction)openDotsSoft:(id)sender {
     [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString: @"https://www.dots-soft.com/"]];
}
- (IBAction)openImageBucket:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString: @"https://www.dots-soft.com/image-bucket/"]];
}
- (IBAction)LisenseAggrement:(id)sender {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"agreement" ofType:@"pdf"];
    [[NSWorkspace sharedWorkspace] openFile:path];
}

@end
