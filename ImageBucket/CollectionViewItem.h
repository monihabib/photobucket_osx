//
//  CollectionViewItem.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PBDefines.h"
#import "PBUserInterfaceProvider.h"

@protocol PBItemSelectionDelegate <NSObject>

-(void)onItemSelected:(NSIndexPath *)indexPath onButton:(BOOL)isButton isSelection:(BOOL)isSelected;

@end


@interface CollectionViewItem : NSCollectionViewItem
@property (readwrite) NSIndexPath *selectionIndexPath;
@property (readwrite) SearchTypes searchType;
@property (readwrite) BOOL isSelected;
@property (readwrite, weak) id<PBItemSelectionDelegate> selectionDelegate;
@property (weak) IBOutlet NSImageView *itemImageView;
@property (weak) IBOutlet NSTextField *size;

@property (weak) IBOutlet NSTextField *name;

@property (weak) IBOutlet NSTextField *resolution;
@property (weak) IBOutlet NSButton *selectImageButton;

@end
