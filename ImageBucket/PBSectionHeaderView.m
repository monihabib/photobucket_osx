//
//  HeaderView.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBSectionHeaderView.h"

@interface PBSectionHeaderView(){
    BOOL isSearchingBusy;
}

@end

@implementation PBSectionHeaderView
@synthesize shouldSelectAll;

-(void)awakeFromNib {
    self.searchTextField.delegate = self;
    self.optionsComboBox.delegate = self;
    self.optionsComboBox.editable = NO;
    isSearchingBusy = NO;
    [self.optionsComboBox selectItemAtIndex:0];
    
}
- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    [[NSColor colorWithCalibratedWhite:0.8 alpha:0.8] set];
    // Drawing code here.
}
- (IBAction)searchingBeginInArray:(id)sender {
    
}

- (IBAction)didSelectComboItem:(id)sender {
    
    self.searchTextField.stringValue = @"";
    NSString *option = self.optionsComboBox.objectValue;
    SearchTypes searchType = [self getSearchTypeFrom: option.lowercaseString];
    [_searchDelegate didSelectedGroupedType:searchType withIndex:(int)self.optionsComboBox.indexOfSelectedItem];
}

-(SearchTypes)getSearchTypeFrom:(NSString *)groupName {
    
    return [groupName containsString:@"folder"] ? GROUPBY_FOLDERNAME : [groupName containsString:@"type"] ?
    GROUPBY_FILETYPE :[groupName containsString:@"none"] ? GROUPBY_NONE :
    [groupName containsString:@"capture"] ? GROUPBY_CAPTUREDATE :
    [groupName containsString:@"comments"] ? GROUPBY_COMMENTS:
    GROUPBY_DUPLICATEIMAGE;
}
- (IBAction)selectAll:(NSButton *)sender {
    shouldSelectAll = !shouldSelectAll;
    [PBUserInterfaceProvider setCheckedImage:sender isChecked:shouldSelectAll];
    [_searchDelegate onAllItemSelected:shouldSelectAll];
}

- (IBAction)deleteAll:(NSButton *)sender {
    [_searchDelegate onDeletionStarted];
}
-(void)setSelectAllPointer:(BOOL **)boolVar{
    (*boolVar) = &shouldSelectAll;
}
@end
