//
//  FileManagerViewController.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/23/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "PBDirectoryExtractor.h"
#import "PBUserInterfaceProvider.h"
#import "PBInfoAlert.h"
#import "PBDeleteManager.h"


@interface PBFileManagerView : NSView



@property (weak) IBOutlet NSComboBox *movingOptionsCombobox;

@property (weak) IBOutlet NSTextField *destinationDirectoryTextField;
@property (weak) IBOutlet NSView *parentContainerView;
@property (weak, readwrite) NSWindow *mainWindow;
@property (readwrite,strong) PBDirectoryExtractor *directoryExtractor;


@end
