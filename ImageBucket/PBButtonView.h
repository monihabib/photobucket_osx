//
//  PBButtonView.h
//  PhotoBucket
//
//  Created by Mostafizur Rahman on 10/27/16.
//  Copyright © 2016 Knowstack. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PBUserInterfaceProvider.h"
#import "PBDefines.h"




@protocol OptionSelectionDelegate;

@interface PBButtonView : NSView
@property (readwrite, nonatomic) NSInteger actionTagValue;
@property (readwrite) BOOL isSelected;
@property (readwrite,weak) id<OptionSelectionDelegate> selectionDelegate;
@end
@protocol OptionSelectionDelegate <NSObject>

-(void)onSelectionCompleted:(PBButtonView *)selectedView;

@end
