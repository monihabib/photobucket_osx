//
//  HeaderView.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PBDefines.h"
#import "PBUserInterfaceProvider.h"

@protocol PBSearchingDelegate <NSObject>

-(void)didFoundSearchedItem:(NSArray *)searchedFoundArray;
-(void)didSelectedGroupedType:(SearchTypes)searchType withIndex:(int)index;
-(void)onDeletionStarted;
-(void)onAllItemSelected:(BOOL)shouldSelectAll;

@end
@interface PBSectionHeaderView : NSView<NSSearchFieldDelegate, NSComboBoxDelegate>
@property (weak) IBOutlet NSImageView *imageView;
@property (weak) IBOutlet NSTextField *headerTitle;
@property (weak) IBOutlet NSTextField *imageCount;
@property (weak) IBOutlet NSComboBox *optionsComboBox;
@property (weak) IBOutlet NSSearchField *searchTextField;
@property (readwrite) NSMutableArray *imagePropertiesArray;
@property (weak) IBOutlet NSTextField *searchResultTextLabel;
@property (readwrite, weak) id<PBSearchingDelegate> searchDelegate;
@property (readwrite) BOOL shouldSelectAll;
-(void)setSelectAllPointer:(BOOL **)boolVar;
@end
