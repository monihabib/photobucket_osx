//
//  ViewController.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreFoundation/CoreFoundation.h>


#import "CollectionViewItem.h"
#import "PBDefines.h"
#import "PBButtonView.h"
#import "PBDirectoryExtractor.h"
#import "PBDetailsView.h"
#import "PBFileProperties.h"

#import "CollectionViewItem.h"
#import "PBInfoAlert.h"

#import "PBFileManagerView.h"
#import "PhotoCollectionView.h"


@interface PBPhotoViewController : NSViewController<
NSWindowDelegate,
NSTextFieldDelegate,
FileManagerDelegate,
OptionSelectionDelegate,
PBCollectionItemSelectionDelegate>
@property (weak) IBOutlet NSSearchField *searchTextField;

@property (weak) IBOutlet PBButtonView *sendImageView;
@property (weak) IBOutlet PBButtonView *deleteImageView;
@property (weak) IBOutlet PBButtonView *previewImageView;

@property (weak) IBOutlet NSScrollView *collectionViewContainer;

@property (weak) IBOutlet PhotoCollectionView *photoCollectionView;

@property (weak) IBOutlet NSTextField *totalImageCount;
@property (weak) IBOutlet NSTextField *duplicateImageCount;

@property (weak) IBOutlet NSView *stackViewContainer;
@property (weak) IBOutlet NSView *bottomViewContainer;
@property (weak) IBOutlet NSView *contentViewContainer;
@property (weak) IBOutlet NSLayoutConstraint *layoutCostraintsLeadingEdgeDetailsView;
@property (weak) IBOutlet NSView *detailsView;
@property (weak) IBOutlet NSButton *rightArrowButton;


@end

