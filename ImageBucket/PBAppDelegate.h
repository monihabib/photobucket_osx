//
//  AppDelegate.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>


#import "PBFileProperties.h"

@interface PBAppDelegate : NSObject <NSApplicationDelegate,NSWindowDelegate>

@property (readwrite) NSURL *thumbFolderURL;

@end

