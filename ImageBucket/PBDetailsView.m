//
//  PBDetailsView.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 12/3/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBDetailsView.h"
#import "PBUserInterfaceProvider.h"
@interface PBDetailsView (){
    NSArray *fileUrlArray;
}

@end

@implementation PBDetailsView


-(void)clearObject{
    self.iconImageView.image = nil;
    self.largeImageView.image = nil;
}

-(void)drawRect:(NSRect)dirtyRect{
    [super drawRect:dirtyRect];
    fileUrlArray = [NSArray arrayWithObjects:_selectedFileProperties.fileUrl, nil];
    [self setImageDescriptions];
}

- (IBAction)showImageInFinder:(NSButton *)sender {
    [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:fileUrlArray];
}

-(void)setImageDescriptions {
#pragma -mark Overview tabs  IB Outlet
    _largeImageView.image = [[NSImage alloc] initWithContentsOfURL:_selectedFileProperties.fileUrl];
    [_largeImageView setImageFrameStyle:NSImageFrameNone];
    _fileNameTextField.stringValue = _selectedFileProperties.fileUrl.lastPathComponent;
    _resolutionTextField.stringValue = [NSString stringWithFormat:FORMAT_RESOLUTION,
                                        _selectedFileProperties.imagePixelWidth,
                                        _selectedFileProperties.imagePixelHeight];
    _fileSizeTextField.stringValue = [NSString stringWithFormat:FORMAT_SIZE,[PBUserInterfaceProvider getDataSizeString:_selectedFileProperties.fileDataLength]];
    
#pragma -mark Details tabs  IB Outlet
    _filenameTextField.stringValue = _selectedFileProperties.fileUrl.lastPathComponent;
    _creationDateTextField.stringValue = [NSString stringWithFormat:FORMAT_DATESTR,
                                          _selectedFileProperties.fileCreationDate.day,
                                          [ARRAY_MONTHNAME objectAtIndex:_selectedFileProperties.fileCreationDate.month - 1],
                                          _selectedFileProperties.fileCreationDate.year];
    _gpsTextField.stringValue = _selectedFileProperties.imageGPSPosition ? _selectedFileProperties.imageGPSPosition : NOGPSDATA;
    _pixelSizeTextField.stringValue = [NSString stringWithFormat:FORMAT_RESOLUTION,
                                       _selectedFileProperties.imagePixelWidth,
                                       _selectedFileProperties.imagePixelHeight];
    _imageSizeTextField.stringValue =  [PBUserInterfaceProvider getDataSizeString:_selectedFileProperties.fileDataLength];
    _cameraModelTextField.stringValue =  [NSString stringWithFormat:FORMAT_DSTR, _selectedFileProperties.softwareName ? _selectedFileProperties.softwareName : EMPTY_STRING, _selectedFileProperties.imageCameraModel ? _selectedFileProperties.imageCameraModel : EMPTY_STRING];
    _commentsTextField.stringValue = _selectedFileProperties.imageHashTag ? _selectedFileProperties.imageHashTag : NOCOMMENT;
    _absulatePathTextField.stringValue = [NSString stringWithFormat:FORMAT_ARG,_selectedFileProperties.fileUrl.path];
    _iconImageView.image = [[NSImage alloc] initWithContentsOfURL:_selectedFileProperties.thumbImageURL];
}

@end
