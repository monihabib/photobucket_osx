//
//  ViewController.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBPhotoViewController.h"


@interface PBPhotoViewController(){
    
    NSMutableArray *rootPanelArray;
    NSMutableArray *buttonArray;
    PBButtonView *selectedButtonView;
    PBDirectoryExtractor *directoryExtractor;
    NSURL *rootDirectoryURL;
    NSView *baseIndicatorView;
    PBFileManagerView *fileManagerView;
    NSView *statusView;
    NSView *noticeView;
    PBDetailsView *imageDetailsView;
    BOOL isDetailsOnScreen;
    BOOL isFileManagerBusy;
    BOOL isSearchingBusy;
    BOOL shouldReloadCollection;
    NSUInteger selectedAction;
    NSButton *dirChooserButton;
    NSTextField *rootDirTextField;
    NSButton *aboutButton;
}
@end
@implementation PBPhotoViewController

-(void)setViewDelegate{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeWindow) name:NSWindowDidResizeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(transitionController) name:NSApplicationDidChangeOcclusionStateNotification object:nil];
    _photoCollectionView.selectionDelegate = self;
    _photoCollectionView.dataSource = _photoCollectionView;
}

-(void)setToolBarActions {
    
    NSToolbar *toolBar = self.view.window.toolbar;
    for(NSToolbarItem *item in toolBar.items){
         if(item.view.tag == TAG_DIRECTORY) {
            dirChooserButton = (NSButton *)item.view;
            [dirChooserButton setTarget:self];
            [dirChooserButton setAction:@selector(openRootDirChooser)];
        }
        else if (item.view.tag == TAG_TEXTFIELD){
            rootDirTextField = (NSTextField *)item.view;
            rootDirTextField.delegate = self;
            _photoCollectionView.rootPathTextField = rootDirTextField;
        }
    }
}

#pragma -mark Init System Delegates.

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setViewDelegate];
    [self setToolBarActions];
}

-(void)loadNibViews {
    NSArray *nibNameArray = [NSArray arrayWithObjects:NIBNAME_STATUS, NIBNAME_NOTICE, NIBNAME_DETAIL, NIBNAME_MANAGE, nil];
    NSMutableArray *viewArray = [[NSMutableArray alloc] init];
    for(NSString *nibName in nibNameArray ){
        NSMutableArray *views;
        [[NSBundle mainBundle] loadNibNamed:nibName owner:self.view topLevelObjects:&views];
        for(id view in views){
            if ([view isKindOfClass:[NSView class]]) {
                [self setRepresentedObject:view];
                [viewArray addObject:view];
                break;
            }
        }
    }
    shouldReloadCollection = YES;
    statusView = [viewArray objectAtIndex:0];
    noticeView = [viewArray objectAtIndex:1];
    imageDetailsView = [viewArray objectAtIndex:2];
    fileManagerView = [viewArray objectAtIndex:3];
    directoryExtractor = [[PBDirectoryExtractor alloc]init];
    directoryExtractor.fileManagerDelegate = self;
    directoryExtractor.statusView = statusView;
    _photoCollectionView.directoryExtractor = directoryExtractor;
    fileManagerView.directoryExtractor = directoryExtractor;
    [PBUserInterfaceProvider addSubview:noticeView toParentView:_contentViewContainer];
}

-(void)initArrays {
    rootPanelArray = [[NSMutableArray alloc] initWithObjects: _contentViewContainer, _bottomViewContainer, _stackViewContainer, nil];
    buttonArray = [[NSMutableArray alloc] initWithObjects: _sendImageView, _deleteImageView, _previewImageView, nil];
}

#pragma -mark Details View configuration and animation
-(void)configureDetailsView{
    _detailsView.translatesAutoresizingMaskIntoConstraints = NO;
    _detailsView.wantsLayer = YES;
    _detailsView.layer.backgroundColor = [[NSColor lightGrayColor] CGColor];
}

-(void)configureAllViews {
    [self configureDetailsView];
    [PBUserInterfaceProvider configureActionInViewArray:buttonArray withDelegate:self];
    [PBUserInterfaceProvider setBackgroundColorToArray:rootPanelArray];
}

-(void)setLayoutValue {
    CGFloat value = isDetailsOnScreen ? 0 : _contentViewContainer.frame.size.width;
    _layoutCostraintsLeadingEdgeDetailsView.constant = value;
    [_detailsView setNeedsDisplay:YES];
}

-(void)viewWillAppear{
    [super viewWillAppear];
    [self loadNibViews];
    [self initArrays];
    [self configureAllViews];
    [self setLayoutValue];
    [self setToolBarActions];
}

-(void)setRepresentedObject:(id)representedObject{
    [super setRepresentedObject:representedObject];
}

-(void)resizeWindow {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setLayoutValue];
        CGSize frameSize = _contentViewContainer.frame.size;
        CGPoint centerPosition = CGPointMake(frameSize.width / 2, frameSize.height / 2);
        baseIndicatorView.frame = CGRectMake(centerPosition.x - MAX_ANIM_WIDTH /2,
                                             centerPosition.y - MAX_ANIM_WIDTH /2,
                                             MAX_ANIM_WIDTH, MAX_ANIM_WIDTH);
    });
}

-(void)transitionController {
    if([[NSApplication sharedApplication] occlusionState] != NSApplicationOcclusionStateVisible){
        [self.photoCollectionView refreshComboBox];
    }
}

#pragma -mark UI Actions
- (IBAction)hideDetailsView:(id)sender {
    [self animateToHideDetailsView];
}
-(void)openAboutUs:(NSButton *)button{
    
    
}

-(void)openRootDirChooser {
    
    NSOpenPanel *openPanel = [[NSOpenPanel alloc] init];
    [openPanel setCanChooseFiles:NO];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setAllowsMultipleSelection:NO];
    [openPanel beginSheetModalForWindow:self.view.window completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSFileHandlingPanelOKButton) {
            if(isDetailsOnScreen){
                [self raiseAlertFrom:ALERTTYPE_HIDEDETAILSVIEW withOptions:nil];
            } else {
                for (rootDirectoryURL in [openPanel URLs]) {
                    rootDirTextField.stringValue = rootDirectoryURL.path;
                    [self onDirectorySelected];
                    break;
                }
            }
        }
    }];
}

-(void)controlTextDidEndEditing:(NSNotification *)notification {
    if ( [[[notification userInfo] objectForKey:KEY_TEXTMOVEMENT] intValue] == NSReturnTextMovement )
    {
        if(isSearchingBusy || isFileManagerBusy){
            [self raiseAlertFrom:ALERTTYPE_WAITUNTIFINISH withOptions:nil];
        }
        else if(isDetailsOnScreen){
            [self raiseAlertFrom:ALERTTYPE_HIDEDETAILSVIEW withOptions:nil];
        }
        else if(rootDirTextField == [notification object]) {
            rootDirectoryURL = [NSURL fileURLWithPath:rootDirTextField.stringValue];
            [self onDirectorySelected];
        }
    }
}

-(void)animateDidEnd {
    [self setLayoutValue];
    if(selectedAction == ACITON_FILEMANAGER) {
        [self addFileManagerView];
    }
}

-(void)animateToShowDetailsView{
    isDetailsOnScreen = YES;
    _photoCollectionView.isDetailsViewActive = YES;
    _rightArrowButton.hidden = !_rightArrowButton.hidden;
    NSRect newFrame = NSMakeRect(0, _detailsView.frame.origin.y,
                                 _detailsView.frame.size.width,
                                 _detailsView.frame.size.height);
    [self performSelector:@selector(animateViewToFrame:)
               withObject:[NSValue valueWithRect:newFrame]
               afterDelay:0];
}

-(void)animateToHideDetailsView{
    _photoCollectionView.isDetailsViewActive = NO;
    isDetailsOnScreen = NO;
    _rightArrowButton.hidden = !_rightArrowButton.hidden;
    NSRect newFrame = NSMakeRect( _contentViewContainer.frame.size.width,
                                 _detailsView.frame.origin.y,
                                 _detailsView.frame.size.width,
                                 _detailsView.frame.size.height);
    [self performSelector:@selector(animateViewToFrame:)
               withObject:[NSValue valueWithRect:newFrame] afterDelay:0];
}

- (void)animateViewToFrame:(NSValue*)frameValue {
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:ANIMATION_DURATION];
    [self performSelector:@selector(animateDidEnd) withObject:nil afterDelay:ANIMATION_DURATION];
    [[_detailsView animator] setFrame:[frameValue rectValue]];
    [NSAnimationContext endGrouping];
}

#pragma -mark Progressing Animation
-(void)startProgressAnimation {
    baseIndicatorView = [[NSView alloc] init];
    [PBUserInterfaceProvider setActivityIndicator:TITLE_LOADING
                                     inParentView:_contentViewContainer
                                           toView:baseIndicatorView];
    [_contentViewContainer addSubview:statusView];
    statusView.frame = NSMakeRect(0, 0, _contentViewContainer.frame.size.width, HEIGHT_STATUSVIEW);
    [PBUserInterfaceProvider setAutolayoutConstraintWithFixedHeightInView:_contentViewContainer toView:statusView];
}

#pragma -mark Show/Manage Alert Views
-(void)raiseAlertFrom:(AlertTypes)alertType withOptions:(NSDictionary *)options{
    PBInfoAlert *alert = [[PBInfoAlert alloc] initWithType:alertType withOptions:options];
    [alert beginSheetModalForWindow:self.view.window completionHandler:^(NSModalResponse returnCode) {
        if(!shouldReloadCollection){
            shouldReloadCollection = returnCode == CODE_OKAY;
            if(shouldReloadCollection){
                [self onDirectorySelected];
            }
            else {
                [self removeAllViews];
                [self reloadPhotoCollectionView];
            }
            shouldReloadCollection = YES;
        }
    }];
}

#pragma -mark All action Delegates
#pragma -mark Button Selection Event
-(void)onSelectionCompleted:(PBButtonView *)selectedView {
    [self updateSelectedButtonInterface:selectedView];
    [self performButtonActionForTag:selectedView.actionTagValue];
}

-(void)updateSelectedButtonInterface:(PBButtonView *)selectedView {
    if (selectedView != selectedButtonView) {
        for (PBButtonView *view in buttonArray) {
            if (selectedView != view) {
                view.isSelected = NO;
            }
        }
        if (selectedButtonView) {
            selectedButtonView.needsLayout = YES;
            selectedButtonView.layer.borderWidth = 0;
        }
        selectedButtonView = selectedView;
        selectedButtonView.needsLayout = YES;
        selectedButtonView.layer.borderColor = [THEME_COLOR CGColor];
        selectedButtonView.layer.borderWidth = BORDER_WIDTH;
    }
}

-(void)performButtonActionForTag:(const NSInteger)actionTag {
    if(isSearchingBusy || isFileManagerBusy){
        [self raiseAlertFrom:ALERTTYPE_WAITUNTIFINISH withOptions:nil];
    }
    else if(!rootDirectoryURL)[self raisDirectoryError];
    else if(!isDetailsOnScreen){
        [self removeAllViews];
        if(actionTag == ACTION_PREVIEWIMAGE) {
            shouldReloadCollection = NO;
            [self raiseAlertFrom:ALERTTYPE_RELOADCOLLECTION withOptions:nil];
            
        }
        else if(actionTag == ACITON_FILEMANAGER) [self addFileManagerView];
        else if(actionTag == ACTION_FILESHARING) [self displayFileManagerStatistics];
    } else { [self raiseAlertFrom:ALERTTYPE_HIDEDETAILSVIEW withOptions:nil]; }
}

-(void)removeAllViews{
    [statusView removeFromSuperview];
    [noticeView removeFromSuperview];
    [fileManagerView removeFromSuperview];
    [baseIndicatorView removeFromSuperview];
    [directoryExtractor.deleteManager.scrollView removeFromSuperview];
}


-(void)getImageProperties {
    _collectionViewContainer.hidden = YES;
    [self removeAllViews];
    [directoryExtractor deleteThumbFolder];
    [self startProgressAnimation];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self processRootDirectory];
    });
}

#pragma -mark CollectionView Item/Cell Selection Delegate
-(void)onItemSelected:(PBFileProperties *)selectedFileProperties{
    [self createDetailsView:selectedFileProperties];
    [self animateToShowDetailsView];
}

-(void)createDetailsView:(PBFileProperties *)selectedProperties {
    [imageDetailsView removeFromSuperview];
    imageDetailsView.largeImageView.image = nil;
    imageDetailsView.iconImageView.image = nil;
    imageDetailsView.selectedFileProperties = selectedProperties;
    [PBUserInterfaceProvider addSubview:imageDetailsView toParentView:_detailsView];
}

#pragma -mark Directory Selection Delegate
-(void)onDirectorySelected{
    if([directoryExtractor isDirectoryUrl:rootDirectoryURL])
        [self getImageProperties];
     else [self raisDirectoryError];
}


#pragma -mark Error handling
-(void)raisDirectoryError {
    [directoryExtractor.imagePropertiesArray removeAllObjects];
    _collectionViewContainer.hidden = YES;
    NSString *path = rootDirectoryURL.path ? rootDirectoryURL.path : TITLE_EMPTY;
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:path forKey:KEY_DIRECTORY];
    [self raiseAlertFrom:ALERTTYPE_INVALIDDIRECTORY withOptions:dictionary];
    [PBUserInterfaceProvider addSubview:noticeView toParentView:_contentViewContainer];
}

#pragma -mark Load image from directory
-(void)processRootDirectory {
    isSearchingBusy = YES;
    _photoCollectionView.isListing = YES;
    [directoryExtractor getFileListFromURL:rootDirectoryURL];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadPhotoCollectionView];
        [self removeAllViews];
        [self setTotalImageCount];
        isSearchingBusy = NO;
    });
}

-(void)setTotalImageCount{
    _duplicateImageCount.textColor = [NSColor blackColor];
    _totalImageCount.textColor = [NSColor blackColor];
    _totalImageCount.stringValue = [NSString stringWithFormat:FORMAT_TOTALIMAGE, directoryExtractor.totalImageCount];
    _duplicateImageCount.stringValue = [NSString stringWithFormat:FORMAT_DUPLICATEIMAGE, directoryExtractor.duplicateCount];
}

-(void)reloadPhotoCollectionView{
    _photoCollectionView.isListing = NO;
    _collectionViewContainer.hidden = NO;
    [_photoCollectionView isConfiguredSearchingWithOptions:@{KEY_SEARCHINDEX : [NSNumber numberWithInt:GROUPBY_INACTIVE]}];
}

#pragma -mark File Manager ViewController

-(void)displayFileManagerStatistics {
    if(directoryExtractor.deleteManager){
        [directoryExtractor.deleteManager performDeletionStatistics:_contentViewContainer];
    }
}

-(void)addFileManagerView {
    if (rootDirectoryURL && [directoryExtractor isDirectoryUrl:rootDirectoryURL]) {
        _collectionViewContainer.hidden = YES;
        [directoryExtractor.deleteManager setRootUrl:rootDirectoryURL.path];
        [PBUserInterfaceProvider addSubview:fileManagerView toParentView:_contentViewContainer];
    } else [self raisDirectoryError];
}

- (IBAction)searchImageInCollectionView:(NSSearchField *)sender {
    if(!isSearchingBusy && !isFileManagerBusy) {
        isSearchingBusy = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSString *searchedString = self.searchTextField.stringValue;
            NSArray *searchedArray;
            if(![searchedString isEqualToString:EMPTY_STRING]){
                NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:SEARCH_PREDICATE, searchedString, searchedString, searchedString, searchedString];
                searchedArray = [directoryExtractor.imagePropertiesArray filteredArrayUsingPredicate:resultPredicate];
            } else {
                searchedArray = directoryExtractor.imagePropertiesArray;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [_photoCollectionView didFoundSearchedItem:searchedArray];
                isSearchingBusy = NO;
            });
        });
    }
}

-(void)onFileManagerBusy {
    isFileManagerBusy = YES;
}

-(void)onMoveCompleted{
    isFileManagerBusy = NO;
    [self onDirectorySelected];
}

-(void)onDeletionCompleted{
    isFileManagerBusy = NO;
    [self onDirectorySelected];
}

-(void)onSelectedDeleteCompleted{
    [_photoCollectionView reloadData];
    _totalImageCount.stringValue = @"";
    _duplicateImageCount.stringValue = @"Need to reload root directory content.";
    _duplicateImageCount.textColor = THEME_COLOR;
}
@end
