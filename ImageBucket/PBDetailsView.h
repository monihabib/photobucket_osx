//
//  PBDetailsView.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 12/3/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PBFileProperties.h"
#import "PBAppDelegate.h"
#import "PBDefines.h"



@interface PBDetailsView : NSView

@property (readwrite) PBFileProperties *selectedFileProperties;


#pragma -mark Overview tabs  IB Outlet
@property (weak) IBOutlet NSImageView *largeImageView;
@property (weak) IBOutlet NSTextField *fileNameTextField;
@property (weak) IBOutlet NSTextField *resolutionTextField;
@property (weak) IBOutlet NSTextField *fileSizeTextField;


#pragma -mark Details tabs  IB Outlet
@property (weak) IBOutlet NSTextField *filenameTextField;
@property (weak) IBOutlet NSTextField *creationDateTextField;
@property (weak) IBOutlet NSTextField *gpsTextField;
@property (weak) IBOutlet NSTextField *pixelSizeTextField;
@property (weak) IBOutlet NSTextField *imageSizeTextField;
@property (weak) IBOutlet NSTextField *cameraModelTextField;
@property (weak) IBOutlet NSTextField *commentsTextField;
@property (weak) IBOutlet NSTextField *absulatePathTextField;
@property (weak) IBOutlet NSImageView *iconImageView;
-(void)clearObject;
@end
