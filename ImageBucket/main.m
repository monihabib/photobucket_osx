//
//  main.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
