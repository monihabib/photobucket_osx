//
//  PBInfoAlert.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/26/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>

#import "PBDefines.h"

@interface PBInfoAlert : NSAlert
-(id)initWithType:(AlertTypes) alertType withOptions:(NSDictionary *)options;
@end
