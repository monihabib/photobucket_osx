//
//  AppDelegate.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/8/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBAppDelegate.h"

@interface PBAppDelegate ()

@end

@implementation PBAppDelegate



- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

-(void)windowDidBecomeMain:(NSNotification *)notification{
    
}
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    [[NSFileManager defaultManager] removeItemAtPath:_thumbFolderURL.path error:nil];
    return YES;
}

-(void)applicationDidBecomeActive:(NSNotification *)notification {
    [[NSApplication sharedApplication] unhide:self];
}
@end
