//
//  PBUserInterfaceProvider.h
//  PhotoBucket
//
//  Created by Mostafizur Rahman on 10/27/16.
//  Copyright © 2016 Knowstack. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import "PBDefines.h"
#import "PBButtonView.h"



@interface PBUserInterfaceProvider : NSObject
+(CALayer *)getBorderLayerWithLength:(const CGFloat)borderLength;

+(void)setMouseExitLook:(NSView *)inputView;
+(void)setMouseClickedLook:(NSView *)inputView;
+(void)setMouseHoverLook:(NSView *)inputView;
+(void)setHighlightedImageView:(NSImageView *)logoImageView
                withTitleLabel:(NSTextField *)titleTextField;
+(void)setDefaultImageView:(NSImageView *)logoImageView
                withTitleLabel:(NSTextField *)titleTextField;
+(void)setAutoLayoutConstraintInView:(NSView *)parentView
                              toView:(NSView *)childView;
+(void)setAutolayoutConstraintWithFixedHeightInView:(NSView *)parentView
                                             toView:(NSView *)childView;
//+(void)setCenterXYAutoLayoutConstraintInView:(NSView *)parentView toView:(NSView *)childView;
+(void)addSubview:(NSView *)subView toParentView:(NSView *)parentViewContainer;
+(void)setActivityIndicator:(NSString *)titleString
               inParentView:(NSView *)_contentViewContainer
                     toView:(NSView *)baseIndicatorView;
+ (NSView *)loadWithNibNamed:(NSString *)nibNamed owner:(id)owner class:(Class)loadClass;


+(void)setBackgroundColorToArray:(NSMutableArray *)rootPanelArray;
+(void)configureActionInViewArray:(NSMutableArray *)buttonArray
                     withDelegate:(id)callBackDelegate;

+(void)setCheckedImage:(NSButton *)button
             isChecked:(BOOL)isChecked;

+(NSTextField *)getTextFiledWith:(CGRect)textFieldFrame
                        forTitle:(NSString *)titleString
                      alignement:(NSTextAlignment)textAlignment
                        tagValeu:(NSUInteger)tag;

+(void)setAutoLayoutConstraintXYCenterTopInView:(NSView *)parentView toView:(NSView *)childView ;
+(void)setAutoLayoutConstraintXYCenterBottomInView:(NSView *)parentView toView:(NSView *)childView;

+(NSString *)getDataSizeString:(long)fileSize;
+(NSString *)getRandomStringOfLen:(NSInteger)length;
+(CALayer *)getBorderLayer:(const CGFloat)borderLength ;
+(BOOL)moveFileUsing:(NSFileManager *)fileManager destinationUrl:(NSURL *)destinationURL sourceUrl:(NSURL *)sourceFileURL error:(NSError*__autoreleasing*)movingError;
@end
