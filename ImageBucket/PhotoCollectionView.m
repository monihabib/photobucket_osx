//
//  PhotoCollectionView.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 12/13/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PhotoCollectionView.h"
#import <AppKit/AppKit.h>


@interface PhotoCollectionView (){
    
    NSUInteger sectionCount;
    NSMutableArray *collectionViewItemArray;
    NSMutableArray *selectedImageArray;
    NSMutableDictionary *catItemDictinary;
    NSArray *catKeyArray;
    NSButton *headerSelectAllButton;
    int selectedIndex;
    BOOL isSearchingActive;
    BOOL isSelectAllClicked;
    BOOL *isSelectAllInHeader;
    SearchTypes searchTypeIndexed;
}

@end

@implementation PhotoCollectionView
@synthesize directoryExtractor;


-(void)awakeFromNib {
    collectionViewItemArray = [[NSMutableArray alloc] init];
    selectedImageArray = [[NSMutableArray alloc] init];
    NSCollectionViewFlowLayout *flowLayout = [[NSCollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = NSMakeSize(200, 200);
    flowLayout.sectionInset = NSEdgeInsetsMake(CONST_SPACING, CONST_SPACING, CONST_SPACING, CONST_SPACING) ;
    flowLayout.minimumInteritemSpacing = CONST_SPACING;
    flowLayout.minimumLineSpacing = CONST_SPACING;
    flowLayout.headerReferenceSize = CGSizeMake(self.frame.size.width, 56.f);
    self.collectionViewLayout = flowLayout;
    searchTypeIndexed = GROUPBY_INACTIVE;
    [self configureSectionHeaderType];
    
    [self registerClass:[CollectionViewItem class] forItemWithIdentifier:IDENTIFIER_ITEM];
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    self.wantsLayer = YES;
    self.layer.backgroundColor = [[NSColor lightGrayColor] CGColor];
}

-(BOOL)resignFirstResponder {
    return _isListing;
}

#pragma -mark CollectionView Delegates and Data Sources

-(NSInteger)collectionView:(NSCollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section{
    if(_isListing) return 0;
    if(searchTypeIndexed > CATEGORY_COUNT) {
        return [collectionViewItemArray count];
    }
    
    id sectionKey = [catKeyArray objectAtIndex:section];
    collectionViewItemArray = [catItemDictinary objectForKey:sectionKey];
    return [collectionViewItemArray count];
}

-(NSInteger)numberOfSectionsInCollectionView:(NSCollectionView *)collectionView{
    if(_isListing)return 0;
    return sectionCount;
}


- (NSCollectionViewItem *)collectionView:(NSCollectionView *)collectionView
     itemForRepresentedObjectAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewItem *collectionItem = [collectionView makeItemWithIdentifier:IDENTIFIER_ITEM forIndexPath:indexPath];
    if(_isListing) return collectionItem;
    collectionItem.selectionIndexPath = indexPath;
    collectionItem.searchType = searchTypeIndexed;
    collectionItem.selectionDelegate = self;
    
    PBFileProperties *fileProperties = [self getFileProperties:indexPath];
    [PBUserInterfaceProvider setCheckedImage:collectionItem.selectImageButton isChecked:fileProperties.isSelected];
    collectionItem.isSelected = fileProperties.isSelected;
    NSImage *thumbImage = [[NSImage alloc] initWithContentsOfURL:fileProperties.thumbImageURL];

    collectionItem.itemImageView.image =  thumbImage;
    collectionItem.name.stringValue = fileProperties.fileName;
    collectionItem.size.stringValue = [NSString stringWithFormat: FORMATTER_FILESIZE,(int)fileProperties.fileDataLength / SIZE_ONEK];
    collectionItem.resolution.stringValue = [NSString stringWithFormat:FORMATTER_RESOLUTION,(int)fileProperties.imagePixelWidth, (int)fileProperties.imagePixelHeight];
    return collectionItem;
}

-(NSSize)collectionView:(NSCollectionView *)collectionView
                 layout:(NSCollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section {
    return NSMakeSize( self.frame.size.width,  56);
}

-(void)setSelectionInProperties:(BOOL)isSelected {
    for(PBFileProperties *fileProperties in selectedImageArray){
        fileProperties.isSelected = isSelected;
    }
}

-(void)onAllItemSelected:(BOOL)isSelected {
    _isSelectAll = isSelected;
    *isSelectAllInHeader = _isSelectAll;
    if(isSelected){
        for(PBFileProperties *fileProperties in directoryExtractor.imagePropertiesArray){
            if(![selectedImageArray containsObject:fileProperties]) {
                [selectedImageArray addObject:fileProperties];
            }
        }
        [self setSelectionInProperties:isSelected];
    } else {
        [self setSelectionInProperties:isSelected];
        [selectedImageArray removeAllObjects];
    }
    isSelectAllClicked = YES;
    [self reloadData];
    [self performBatchUpdates:^{
        
    } completionHandler:^(BOOL finished) {
        isSelectAllClicked = NO;
    }];
}


#pragma -mark Selection Delegate

-(void)onItemSelected:(NSIndexPath *)indexPath
             onButton:(BOOL)isButton
          isSelection:(BOOL)isSelected {
    
    PBFileProperties *fileProperties;
    if(searchTypeIndexed > CATEGORY_COUNT)
    {
        fileProperties = [collectionViewItemArray objectAtIndex:indexPath.item];
    }
    else
    {
        fileProperties = [self getFileProperties:indexPath];
    }
    if(isButton )
    {
        fileProperties.isSelected = isSelected;
        if(isSelected)
        {
            [selectedImageArray addObject:fileProperties];
        }
        else
        {
            [selectedImageArray removeObject:fileProperties];
        }
        _isSelectAll = [directoryExtractor.imagePropertiesArray count] == [selectedImageArray count];
        *isSelectAllInHeader = _isSelectAll;
        [PBUserInterfaceProvider setCheckedImage:headerSelectAllButton isChecked:_isSelectAll];
    }
    else
    {
        if(!_isDetailsViewActive)
        {
            [_selectionDelegate onItemSelected:fileProperties];
        }
    }
}


-(NSView *)collectionView:(NSCollectionView *)collectionView
viewForSupplementaryElementOfKind:(NSString *)kind
              atIndexPath:(NSIndexPath *)indexPath{
    PBSectionHeaderView *headerView = [collectionView makeSupplementaryViewOfKind:kind
                                                                   withIdentifier:NIBNAME_HEADER
                                                                     forIndexPath:indexPath];
    if (indexPath.section == ZERO) {
        headerView.imagePropertiesArray = directoryExtractor.imagePropertiesArray;
        headerView.searchDelegate = self;
        headerSelectAllButton = [headerView viewWithTag:TAG_SELECTALL];
        [PBUserInterfaceProvider setCheckedImage:headerSelectAllButton isChecked:_isSelectAll];
        headerView.shouldSelectAll = _isSelectAll;
        if(isSearchingActive){
            selectedIndex = ZERO;
        }
        isSearchingActive = NO;
        [headerView.optionsComboBox selectItemAtIndex:selectedIndex];
        [headerView setSelectAllPointer:&isSelectAllInHeader];
    }
    [self configureSectionHeaderUI:headerView inIndexPath:(NSIndexPath *)indexPath];
    headerView.wantsLayer = YES;
    CALayer *bottomLineLayer  = [PBUserInterfaceProvider getBorderLayer:[NSScreen mainScreen].frame.size.width];
    [headerView.layer insertSublayer:bottomLineLayer atIndex:ZERO];
    return headerView;
}


-(BOOL)isConfiguredSearchingWithOptions:(NSDictionary *)searchingOptions {
    searchTypeIndexed = [[searchingOptions objectForKey:KEY_SEARCHINDEX] intValue];
    [self configureSectionHeaderType];
    return YES;
}

-(void)hideHeaderViewControls:(BOOL)isHidden inHeader:(PBSectionHeaderView *)headerView {
    headerView.searchTextField.hidden = isHidden;
    headerView.optionsComboBox.hidden = isHidden;
    [headerView viewWithTag:TAG_SELECTALL].hidden = isHidden;
    [headerView viewWithTag:TAG_DELETEALL].hidden = isHidden;
}

-(void)configureSectionHeaderUI:(PBSectionHeaderView *)headerView
                    inIndexPath:(NSIndexPath *)indexPath{
    headerView.imageView.image = nil;
    headerView.headerTitle.alignment = NSTextAlignmentLeft;
    [self hideHeaderViewControls:indexPath.section != ZERO inHeader:headerView];
    switch (searchTypeIndexed) {
        case GROUPBY_NONE:
        case GROUPBY_INACTIVE:
            [self configureHeaderGroupedByNone:headerView inIndexPath:indexPath];
            break;
        case GROUPBY_FOLDERNAME:
            [self configureHeaderGroupedByFolder:headerView inIndexPath:indexPath];
            break;
        case GROUPBY_FILETYPE:
        case GROUPBY_CAPTUREDATE:
            [self configureHeaderGroupedByDate:headerView inIndexPath:indexPath];
            break;
        case GROUPBY_DUPLICATEIMAGE:
            [self configureHeaderGroupedByDuplicate:headerView inIndexPath:indexPath];
            break;
        case GROUPBY_COMMENTS:
            [self configureHeaderGroupedByComments:headerView inIndexPath:indexPath];
        default:
            break;
    }
}

-(void)configureHeaderGroupedByComments:(PBSectionHeaderView *)headerView
                            inIndexPath:(NSIndexPath *)indexPath{
    NSMutableString *key = [catKeyArray objectAtIndex:indexPath.section];
    NSUInteger imageCount = [[catItemDictinary objectForKey:key] count];
    headerView.imageCount.stringValue = [NSString stringWithFormat:@"Total images %ld", imageCount];
    headerView.headerTitle.stringValue = key;
}

-(void)configureHeaderGroupedByNone:(PBSectionHeaderView *)headerView
                        inIndexPath:(NSIndexPath *)indexPath {
    
    headerView.headerTitle.stringValue = @"All Images";
    headerView.imageCount.stringValue = [NSString stringWithFormat:@"Total images %ld", [directoryExtractor.imagePropertiesArray count]];
}


-(void)configureHeaderGroupedByDate:(PBSectionHeaderView *)headerView
                        inIndexPath:(NSIndexPath *)indexPath {
    NSMutableString *key = [catKeyArray objectAtIndex:indexPath.section];
    headerView.headerTitle.stringValue = [NSString stringWithFormat:@"%@",key];
    NSUInteger imageCount = [[catItemDictinary objectForKey:key] count];
    headerView.imageCount.stringValue = [NSString stringWithFormat:@"Total images %ld", imageCount];
}

-(void)configureHeaderGroupedByDuplicate:(PBSectionHeaderView *)headerView
                             inIndexPath:(NSIndexPath *)indexPath {
    NSNumber *numberKey = [catKeyArray objectAtIndex:indexPath.section];
    NSMutableArray *filePropertiesArray = [catItemDictinary objectForKey:numberKey];
    PBFileProperties *properties = [filePropertiesArray firstObject];
    headerView.headerTitle.alignment = NSTextAlignmentCenter;
    headerView.headerTitle.stringValue = properties.fileName;
    headerView.imageView.image = [[NSImage alloc] initWithContentsOfURL:properties.thumbImageURL];
    headerView.imageCount.stringValue = [NSString stringWithFormat:@"Total images %ld", [filePropertiesArray count]];
}

-(void)configureHeaderGroupedByFolder:(PBSectionHeaderView *)headerView
                          inIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableString *key = [catKeyArray objectAtIndex:indexPath.section];
    NSUInteger imageCount = [[catItemDictinary objectForKey:key] count];
    headerView.imageCount.stringValue = [NSString stringWithFormat:@"Total images %ld", imageCount];
    
    key = (NSMutableString *)[key stringByReplacingOccurrencesOfString:_rootPathTextField.stringValue withString:@""];
    headerView.headerTitle.stringValue = [NSString stringWithFormat:@"~/%@/*",key];
}



-(void)configureSectionHeaderType {   
    
    if(searchTypeIndexed > CATEGORY_COUNT){
        sectionCount = 1;
        collectionViewItemArray = directoryExtractor.imagePropertiesArray;
        catKeyArray = nil;
    }
    else {
        catItemDictinary = [directoryExtractor.categoryArray objectAtIndex:searchTypeIndexed];
        catKeyArray = [catItemDictinary allKeys];
        sectionCount = [catKeyArray count];
    }
    [self reloadData];
}


#pragma -mark Searching delegate

-(void)didFoundSearchedItem:(NSArray *)searchedFoundArray{
    collectionViewItemArray = (NSMutableArray *)searchedFoundArray;
    isSearchingActive = YES;
    searchTypeIndexed = GROUPBY_NONE;
    sectionCount = 1;
    [self reloadData];
}

-(void)didSelectedGroupedType:(SearchTypes)searchType withIndex:(int)index{
    selectedIndex = index;
    searchTypeIndexed = searchType;
    [self configureSectionHeaderType];
}


-(PBFileProperties *)getFileProperties:(NSIndexPath *)indexPath{
    
    if (searchTypeIndexed > CATEGORY_COUNT) {
        PBFileProperties *fileProperties = [collectionViewItemArray objectAtIndex:indexPath.item];
        return fileProperties;
    }
    id key = [catKeyArray objectAtIndex:indexPath.section];
    NSMutableArray *collection = [catItemDictinary objectForKey:key];
    return [collection objectAtIndex:indexPath.item];
}

-(void)onDeletionStarted {
    
    //start animation
    [self addStatusView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [directoryExtractor deleteSelectedImages];
        [selectedImageArray removeAllObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self configureSectionHeaderType];
            [directoryExtractor.fileManagerDelegate onSelectedDeleteCompleted];
            [self reloadData];
            [self.directoryExtractor.statusView removeFromSuperview];
        });
    });
}

-(void)addStatusView{
    [[self superview] addSubview:self.directoryExtractor.statusView];
    self.directoryExtractor.statusView.frame = NSMakeRect(0, 0, [self superview].frame.size.width, HEIGHT_STATUSVIEW);
    [PBUserInterfaceProvider setAutolayoutConstraintWithFixedHeightInView:[self superview] toView:self.directoryExtractor.statusView];
}

-(void)refreshComboBox {
    for(NSView *view in self.subviews){
        if([view isKindOfClass:[PBSectionHeaderView class]]){
            for(NSView *combobox in view.subviews){
                if([combobox isKindOfClass:[NSComboBox class]]){
                    id ax = NSAccessibilityUnignoredDescendant(combobox);
                    [ax accessibilitySetValue: [NSNumber numberWithBool: NO] forAttribute: NSAccessibilityExpandedAttribute];
                    break;
                }
            }
            break;
        }
    }
}

@end
