//
//  PBDirectoryExtractor.h
//  SameImageCutter
//
//  Created by Mostafizur Rahman on 10/17/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import "PBFileProperties.h"
#import "NSMutableArray+QueueStack.h"
#import "PBDefines.h"
#import "PBAppDelegate.h"
#import "PBDeleteManager.h"





@protocol FileManagerDelegate
-(void)onMoveCompleted;
-(void)onFileManagerBusy;
-(void)onDeletionCompleted;
-(void)onSelectedDeleteCompleted;

@end

@interface PBDirectoryExtractor : NSObject

@property (readwrite, weak) id<FileManagerDelegate> fileManagerDelegate;
@property (readwrite) NSView *statusView;
@property (readwrite) PBDeleteManager *deleteManager;
@property (readonly) NSMutableArray *categoryArray;
@property (readonly) NSMutableArray *imagePropertiesArray;
@property (readwrite) long duplicateCount;
@property (readwrite) long totalImageCount;

-(NSURL *)createFolderIn:(const NSURL *)sourceURL withFolderName:(NSString *)folderName  thumbnail:(BOOL)isThumbnail ;
-(BOOL)isDirectoryUrl:(NSURL *)url;
-(NSArray *)getSubdirectoriesFromURL:(NSURL *)rootDirectoryURL
                           withError:(NSError *__autoreleasing*)error;



-(void)deleteThumbFolder;

-(void)deleteSelectedImages;
-(void)getFileListFromURL:(NSURL *)url; // sub directory list with all images
-(NSMutableArray *)deleteDuplicateFiles;
-(void)moveFilesAccordingToDateTime:(const NSURL *)destinationURL;
-(void)moveCommentedFiles:(const NSURL *)destinationURL;
-(void)moveFilesAccordingToFolderHierarchy:(const NSURL *)destinationURL;
@end
