//
//  PBDirectoryExtractor.m
//  SameImageCutter
//
//  Created by Mostafizur Rahman on 10/17/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBDirectoryExtractor.h"

@interface PBDirectoryExtractor(){
    NSURL *rootDirectoryURL;
    NSURL *thumbPath;
    NSFileManager *fileManager;
    NSMutableArray *subdirectoryInRootArray;
    NSMutableDictionary *commentDictionary;
    NSMutableDictionary *folderCatDictionary;
    NSMutableDictionary *capDateCatDictionary;
    NSMutableDictionary *fileTypeCatDictionary;
    NSMutableDictionary *duplicateCatDictionary;
}

@end
@implementation PBDirectoryExtractor

@synthesize fileManagerDelegate, imagePropertiesArray, categoryArray, deleteManager;

-(id)init{
    self = [super init];
    fileManager= [NSFileManager defaultManager];
    deleteManager = [[PBDeleteManager alloc] init];
    imagePropertiesArray = [[NSMutableArray alloc] init];
    subdirectoryInRootArray = [[NSMutableArray alloc] init];
    duplicateCatDictionary = [[NSMutableDictionary alloc] init];
    fileTypeCatDictionary = [[NSMutableDictionary alloc] init];
    capDateCatDictionary = [[NSMutableDictionary alloc] init];
    folderCatDictionary = [[NSMutableDictionary alloc] init];
    commentDictionary = [[NSMutableDictionary alloc] init] ;
    categoryArray = [NSMutableArray arrayWithObjects:capDateCatDictionary,
                     folderCatDictionary, fileTypeCatDictionary,commentDictionary, duplicateCatDictionary, nil];
    return self;
}

-(NSArray *)getSubdirectoriesFromURL:(NSURL *)rootDirURL
                           withError:(NSError *__autoreleasing*)error {
    
    return [fileManager contentsOfDirectoryAtURL:rootDirURL
                      includingPropertiesForKeys:nil
                                         options:(NSDirectoryEnumerationSkipsHiddenFiles)
                                           error:error];
}

-(BOOL)isDirectoryUrl:(NSURL *)url {
    NSNumber *isDirectory;
    BOOL success = [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:nil];
    return success && [isDirectory boolValue];
}

-(void)getFileListFromURL:(NSURL *)rootDirUrl {
    [imagePropertiesArray removeAllObjects];
    [subdirectoryInRootArray removeAllObjects];
    for(NSMutableDictionary *catDictionary in categoryArray) {
        [catDictionary removeAllObjects];
    }
    NSMutableArray *stackArray = [[NSMutableArray alloc] init];
    [self setThumbURLWith:rootDirUrl];
    self.duplicateCount = ZERO;
    self.totalImageCount = ZERO;
    NSError *error = nil;
    NSArray *rootSubdirectoryArray = [self getSubdirectoriesFromURL:rootDirUrl withError:&error];
    subdirectoryInRootArray = (NSMutableArray *)rootSubdirectoryArray;
    [self setUrlList:rootSubdirectoryArray withStack:stackArray];
    [self setStatusTitle:TITLE_STATUS];
    while(!stackArray.empty) {
        NSURL *fileURL = [stackArray pop];
        if ([self isDirectoryUrl:fileURL]) {
            if([fileURL.path.lowercaseString containsString:FOLDERNAME_THUMB.lowercaseString]){
                continue;
            }
            NSArray *subdirectoryArray = [self getSubdirectoriesFromURL:fileURL withError:&error];
            [self setUrlList:subdirectoryArray withStack:stackArray];
            continue;
        }
        [self setFileList:fileURL];
    }
    if([[duplicateCatDictionary objectForKey:KEY_SINGLEINSTANCE] count] == ZERO){
        [duplicateCatDictionary removeObjectForKey:KEY_SINGLEINSTANCE];
    }
}

-(void)setThumbURLWith:(NSURL *)rootDirUrl{
    rootDirectoryURL = rootDirUrl;
    thumbPath = [self createFolderIn:rootDirUrl withFolderName:FOLDERNAME_THUMB thumbnail:YES];
    ((PBAppDelegate *)[[NSApplication sharedApplication] delegate]).thumbFolderURL = thumbPath;
}

-(void)setUrlList:(NSArray const *)array
        withStack:(NSMutableArray *)stackArray {
    for(NSURL *fileUrl in array) {
        if ([self isDirectoryUrl:fileUrl]) {
            [stackArray addObject:fileUrl];
        }
        else if([IMAGE_TYPE containsObject:fileUrl.pathExtension.lowercaseString]){
            [self setFileList:fileUrl];
        }
    }
}

-(void)setFileList:(NSURL *)fileUrl {
    
    PBFileProperties *fileProperties = [[PBFileProperties alloc] initWithUrl:fileUrl thumb:thumbPath];
    long index = [PBAlgorithms getSearchIndex:imagePropertiesArray withData:fileProperties.fileDataLength];
    [imagePropertiesArray insertObject:fileProperties atIndex:index];
    [self setStatusWith:fileProperties];
    [self groupingProperties:fileProperties];
    [self groupDuplicateFor:fileProperties inIndex:index];
}

#pragma -mark Combobox item category
-(void)groupingProperties:(const PBFileProperties *)properties {
    
    const NSString *directoryPath = [[properties.fileUrl URLByDeletingLastPathComponent] path];
    const NSString *fileType = properties.fileType;
    const NSString *creationDate = [NSString stringWithFormat:FORMAT_DATESTR,
                                    properties.fileCreationDate.day,
                                    [ARRAY_MONTHNAME objectAtIndex:properties.fileCreationDate.month - MINIMUM_COUNT],
                                    properties.fileCreationDate.year];
    const NSArray *keyArray = [NSArray arrayWithObjects:creationDate, directoryPath, fileType, properties.imageHashTag, nil];
    for(id key in keyArray){
        NSMutableDictionary *dictionary = [categoryArray objectAtIndex:[keyArray indexOfObject:key]];
        NSArray *allKeys = [dictionary allKeys];
        if ([allKeys containsObject:key]) {
            NSMutableArray *capDateGroupArray = [dictionary objectForKey:key];
            if (![capDateGroupArray containsObject:properties]) {
                [capDateGroupArray addObject:properties];
            }
        } else {
            NSMutableArray *capDateGroupArray = [[NSMutableArray alloc] init];
            [capDateGroupArray addObject:properties];
            [dictionary setObject:capDateGroupArray forKey:key];
        }
    }
}


-(void)groupDuplicateFor:(PBFileProperties *)properties inIndex:(NSInteger )index {
    
    NSRange range = NSMakeRange(index, imagePropertiesArray.count - index);
    NSArray *duplicateSubArray = [imagePropertiesArray subarrayWithRange:range];
    NSMutableArray *duplicateFilterArray = [[NSMutableArray alloc] init];
    for(PBFileProperties *fileProperties in duplicateSubArray){
        if(fileProperties.fileDataLength != properties.fileDataLength) break;
        [duplicateFilterArray addObject:fileProperties];
    }
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:FORMAT_LENPREDICATE, @(properties.fileDataLength)];
    if([duplicateFilterArray count] > MINIMUM_COUNT) {
        NSMutableArray *singleInstanceArray = [duplicateCatDictionary objectForKey:KEY_SINGLEINSTANCE];
        NSArray *singleArray = [singleInstanceArray filteredArrayUsingPredicate:resultPredicate];
        if([singleArray count] > ZERO){
            properties.isDuplicate = YES;
            [singleInstanceArray removeObject:[singleArray firstObject]];
            NSMutableArray *duplicatesArray = [NSMutableArray arrayWithObjects:[singleArray firstObject],properties, nil];
            [duplicateCatDictionary setObject:duplicatesArray forKey:[NSNumber numberWithInteger:properties.fileDataLength]];
            self.duplicateCount++;
            return;
        }
        NSArray *allkeys = [duplicateCatDictionary allKeys];
        for(id key in allkeys){
            if([key isKindOfClass:[NSString class]]) continue;
            if ([key integerValue] == properties.fileDataLength) {
                properties.isDuplicate = YES;
                NSMutableArray *duplicatesArray = [duplicateCatDictionary objectForKey:key];
                [duplicatesArray addObject:properties];
                self.duplicateCount++;
                break;
            }
        }
    } else {
        NSMutableArray *singleInstanceArray = [duplicateCatDictionary objectForKey:KEY_SINGLEINSTANCE];
        if([singleInstanceArray count] > ZERO) {
            [singleInstanceArray addObject:properties];
        } else {
            singleInstanceArray = [[NSMutableArray alloc] init];
            [singleInstanceArray addObject:properties];
            [duplicateCatDictionary setObject:singleInstanceArray forKey:KEY_SINGLEINSTANCE];
        }
    }
}

#pragma -mark Status View Information
-(void)setStatusWith:(PBFileProperties *)fileProperties{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setFileStatus:fileProperties];
    });
}

-(void)setStatusTitle:(NSString *)titleString {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSTextField *urlTextField = [_statusView viewWithTag:TAG_TITLEHEADER];
        urlTextField.stringValue = titleString;
        NSTextField *pathTextField = [_statusView viewWithTag:TAG_URLTITLE];
        if ([titleString containsString:DELETETITLE]) {
            pathTextField.stringValue = DELETEMSG;
        } else if([titleString containsString:REDUNDENTTITLE]) {
            pathTextField.stringValue = REDUNENTMSG;
        }
    });
}

-(void)setFileStatus:(PBFileProperties *)fileProperties {
    NSImageView *imageView = [_statusView viewWithTag:NOTICE_IMAGE_TAG];
    imageView.image = [[NSImage alloc] initWithContentsOfURL:fileProperties.thumbImageURL];
    NSTextField *urlTextField = [_statusView viewWithTag:TAG_URLTITLE];
    urlTextField.stringValue = [self replacePercent:fileProperties.fileUrl.absoluteString];
    NSTextField *fileSizeTextField = [_statusView viewWithTag:TAG_NOTICETITLE];
    fileSizeTextField.stringValue = [PBUserInterfaceProvider getDataSizeString: fileProperties.fileDataLength];
    NSTextField *countTextField = [_statusView viewWithTag:TAG_IMAGECOUNT];
    countTextField.stringValue = [NSString stringWithFormat:FORMAT_COUNT, ++self.totalImageCount ]; ;
}



#pragma -mark File/Folder Deletion Duplication Move Handling
-(void)deleteThumbFolder{
    if(thumbPath && [self isDirectoryUrl:thumbPath]) [fileManager removeItemAtPath:thumbPath.path error:nil];
}

-(NSString *)replacePercent:(NSString *)path {
    return [path stringByReplacingOccurrencesOfString:FORMAT_URLSPACE withString:FORMAT_UTFSPACE];
}

-(BOOL)removeItemAt:(NSURL *)removeImageURL {
    return [fileManager removeItemAtURL:removeImageURL error:nil];
}

-(void)moveFilesAccordingToDateTime:(const NSURL *)destinationURL {
    [self setStatusTitle:TITLE_MOVING];
    for(PBFileProperties *fileProperties in imagePropertiesArray) {
        [self moveFileFrom:fileProperties toURL:destinationURL];
    }
    [self cleanImageCollections];
}

-(void)moveFileFrom:(PBFileProperties *)fileProperties toURL:(const NSURL *)destinationURL {
    
    NSString *year = [NSString stringWithFormat:FORMAT_LONG, fileProperties.fileCreationDate.year];
    NSURL *modifiedURL = [self createFolderIn:destinationURL withFolderName:year thumbnail:NO];
    NSString *monthName = [ARRAY_MONTHNAME objectAtIndex:fileProperties.fileCreationDate.month - MINIMUM_COUNT];
    modifiedURL = [self createFolderIn:modifiedURL withFolderName:
                   [NSString stringWithFormat:FORMAT_MMDD, monthName, year] thumbnail:NO];
    modifiedURL = [self createFolderIn:modifiedURL withFolderName:
                   [NSString stringWithFormat:FORMAT_FOLDER,fileProperties.fileCreationDate.day, monthName, year] thumbnail:NO];
    modifiedURL = [modifiedURL URLByAppendingPathComponent:[self replacePercent:fileProperties.fileName] isDirectory:NO];
    if(!modifiedURL){
        NSLog(@"destinatio nil");
    }
    if([self moveFileFrom:fileProperties.fileUrl toDestination:modifiedURL]) {
        [self setStatusWith:fileProperties];
    }
}

-(BOOL)moveFileFrom:(NSURL *)sourceFileURL toDestination:(NSURL *)destinationURL {
    NSError *movingError;
    if([fileManager moveItemAtURL:sourceFileURL toURL:destinationURL error:&movingError]) return YES;
    NSError *error = [movingError.userInfo objectForKey:KEY_UNDERROR];
    if (error.code == ERRORCODE_FILEEXIST) {
        return [PBUserInterfaceProvider moveFileUsing:fileManager destinationUrl:destinationURL sourceUrl:sourceFileURL error:&movingError];
    }
    return NO;
}

-(NSURL *)createFolderIn:(const NSURL *)sourceURL withFolderName:(NSString *)folderName thumbnail:(BOOL)isThumbnail {
    NSString *path = sourceURL.path;
    NSURL *modifiedULR = [NSURL fileURLWithPath:path];
    folderName = [self replacePercent:folderName];
    modifiedULR = [modifiedULR URLByAppendingPathComponent:folderName];
    if(isThumbnail) [fileManager removeItemAtURL:modifiedULR error:nil];
    if (![self isDirectoryUrl:modifiedULR]) {
        NSError *error = nil;
        if(![fileManager createDirectoryAtPath:modifiedULR.path withIntermediateDirectories:YES attributes:nil error:&error]) return nil;
    }
    return modifiedULR;
}

-(void)moveFilesAccordingToFolderHierarchy:(const NSURL *)destinationURL {
    NSDictionary *folderNameDictionary = [categoryArray objectAtIndex:GROUPBY_FOLDERNAME];
    NSArray *folderArray = [folderNameDictionary allKeys];
    for(NSString *folderPath in folderArray) {
        NSURL *folderUrl = [self createHierarchyFolderIn:destinationURL withPath:folderPath];
        [self moveInFolder:folderUrl fileArray:[folderNameDictionary objectForKey:folderPath]];
    }
}

-(void)moveCommentedFiles:(const NSURL *)destinationURL {
    [self setStatusTitle:TITLE_MOVING];
    NSDictionary *imageCommentDictionary = [categoryArray objectAtIndex:GROUPBY_COMMENTS];
    NSArray *commentKey = [imageCommentDictionary allKeys];
    for(NSString *comments in commentKey) {
        NSMutableArray *propertyArray = [imageCommentDictionary objectForKey:comments];
        if([propertyArray count] == 0) continue;
        NSURL *folderUrl = [self createFolderIn:destinationURL withFolderName:comments thumbnail:NO];
        [self moveInFolder:folderUrl fileArray:propertyArray];
    }
}

-(NSURL *)createHierarchyFolderIn:(const NSURL *)destinationURL withPath:(const NSString *)filePath {
    NSString *lastPath = [filePath stringByReplacingOccurrencesOfString:rootDirectoryURL.path withString:EMPTY_STRING];
    NSString *path = [NSString stringWithFormat:FORMAT_DSTR, destinationURL.path, lastPath];
    path = [self replacePercent:path];
    NSURL *modifiedUrl = [NSURL fileURLWithPath:path];
    if(![self isDirectoryUrl:modifiedUrl]){
        return [fileManager createDirectoryAtPath:modifiedUrl.path withIntermediateDirectories:YES attributes:nil error:nil] ? modifiedUrl : nil;
    }
    return modifiedUrl;
}

-(void)moveInFolder:(NSURL *)folderUrl fileArray:(NSMutableArray *)imagePropertyArray {
    if(folderUrl) {
        for(PBFileProperties *fileProperties in imagePropertyArray) {
            if(fileProperties.isDeleted || fileProperties.isMoved) continue;
            NSURL *imageFileUrl = [folderUrl URLByAppendingPathComponent:fileProperties.fileName];
            fileProperties.isMoved = [self moveFileFrom:fileProperties.fileUrl toDestination:imageFileUrl];
        }
    }
}

-(NSMutableArray *)deleteDuplicateFiles {
    [self setStatusTitle:TITLE_DELETE];
    NSMutableArray *singleInstanceArray = [[NSMutableArray alloc] init];
    NSArray *duplicateKeysArray = [duplicateCatDictionary allKeys];
    for(id fileLength in duplicateKeysArray ){
        if([fileLength isKindOfClass:[NSString class]]) continue;
        NSArray *imageArray = [duplicateCatDictionary objectForKey:fileLength];
        [singleInstanceArray addObject:[imageArray firstObject]];
        if([imageArray count] > MINIMUM_COUNT){
            for(NSUInteger duplicateIndex = MINIMUM_COUNT;  duplicateIndex < [imageArray count]; duplicateIndex++) {
                PBFileProperties *duplicateFile = [imageArray objectAtIndex:duplicateIndex];
                duplicateFile.isDeleted = [self removeItemAt: duplicateFile.fileUrl];
                if (duplicateFile.isDeleted) {
                    [self setStatusWith:duplicateFile];
                    [deleteManager updateDeletionStatistics:duplicateFile];
                }
            }
        }
    }
    [self cleanImageCollections];
    return singleInstanceArray;
}

-(void)deleteSelectedImages{
    for(PBFileProperties *imageProperties in imagePropertiesArray){
        if(imageProperties.isSelected){
            imageProperties.isDeleted = [self removeItemAt: imageProperties.fileUrl];
            if (imageProperties.isDeleted) {
                [self setStatusWith:imageProperties];
            }            
        }
    }
    [self cleanImageCollections];
}

-(void)cleanImageCollections{
    self.duplicateCount = 0;
    [self removeObjectFromArrray:imagePropertiesArray];
    for(NSMutableDictionary *actDic in categoryArray){
        NSArray *keyArray = [actDic allKeys];
        for(id key in keyArray){
            NSMutableArray *catArr = [actDic objectForKey:key];
            [self removeObjectFromArrray:catArr];
        }
    }
    self.totalImageCount = [imagePropertiesArray count];
}


-(void)removeObjectFromArrray:(const NSMutableArray *)array{
    long count = [array count];
    for(long index = 0; index < count; index++){
        PBFileProperties *fileProperties = [array objectAtIndex:index];
        if(fileProperties.isDeleted || fileProperties.isMoved){
            [array removeObject:fileProperties];
            index--;
            count--;
        }
    }
}
@end
