//
//  PBDeleteManager.m
//  ImageBucket
//
//  Created by Mostafizur Rahman on 12/28/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBDeleteManager.h"

@interface PBDeleteManager() {
    NSMutableArray *currentPropertiesArray;
    NSView *contentView;
    NSTextField *statTitelTextView;
    NSTextField *dataTextField;
}

@end

@implementation PBDeleteManager

-(instancetype)init{
    self = [super init];
    if(self) {
        _deletedFilePropertiesDictionary = [[NSMutableDictionary alloc] init];
        _rootDirUrlArray = [[NSMutableArray alloc] init];
        _totalSpaceFreed = 0;
        _numberOfFilesDeleted = 0;
        currentRootDirectory = EMPTY_STRING;
    }
    return self;
}

-(void)setRootUrl:(NSString *)rootDirPath {
    isRootDirAlreadyExist = [rootDirPath containsString:currentRootDirectory];
    if(!isRootDirAlreadyExist) {
        currentRootDirectory = rootDirPath;
        [_rootDirUrlArray addObject:rootDirPath];
        currentPropertiesArray = [[NSMutableArray alloc] init];
        [_deletedFilePropertiesDictionary setObject:currentPropertiesArray forKey:rootDirPath];
    }
    else {
        for(NSString *path in _rootDirUrlArray)
        {
            if([path isEqualToString:currentRootDirectory]) {
                currentRootDirectory = path;
                currentPropertiesArray = [_deletedFilePropertiesDictionary objectForKey:path];
                break;
            }
        }
    }
    
}
-(void)updateDeletionStatistics:(PBFileProperties *)fileProperties {
    
    PBDeleteProperties *deleteProperties = [[PBDeleteProperties alloc] initWithProperties:fileProperties];
    [currentPropertiesArray addObject:deleteProperties];
    _numberOfFilesDeleted++;
    _totalSpaceFreed = _totalSpaceFreed + fileProperties.fileDataLength;
}

-(void)performDeletionStatistics:(NSView *)statisctisView {
    if(!_scrollView) {
        _scrollView = [[NSScrollView alloc] initWithFrame:statisctisView.bounds];
        contentView = [[NSView alloc] initWithFrame:statisctisView.bounds];
        [_scrollView setDocumentView:contentView];
        
        [_scrollView setHasVerticalScroller:YES];
        [_scrollView setAutoresizingMask:NSViewWidthSizable|NSViewHeightSizable];
    }
    else {
        NSArray *subViews = [contentView subviews] ;
        NSUInteger count = subViews.count;
        while(count--){
            NSView *view = [subViews lastObject];
            [view removeFromSuperview];
        }
        _scrollView.frame = statisctisView.bounds;
    }
    [statTitelTextView removeFromSuperview];
    [dataTextField removeFromSuperview];
    [statisctisView addSubview:_scrollView];
    [PBUserInterfaceProvider setAutoLayoutConstraintInView:statisctisView toView:_scrollView];
    NSUInteger tag = TAG_STATTEXT;
    CGFloat yCoordinate = MINIMUM_SPACING;
    CGRect titleTextRect = CGRectMake(statisctisView.frame.size.width / 2 - 100, MINIMUM_SPACING, 200, MINIMUM_SPACING);
    statTitelTextView = [PBUserInterfaceProvider getTextFiledWith:titleTextRect
                                                                  forTitle:TITLE_DELSTAT
                                                                alignement:NSTextAlignmentCenter
                                                                  tagValeu:tag++];
    [statTitelTextView setFont:[NSFont fontWithName:FONT_HELVATICA size:18]];
    
    titleTextRect = CGRectMake(statisctisView.frame.size.width / 2 - 100, 40, 200, 15);

    dataTextField = [PBUserInterfaceProvider getTextFiledWith:titleTextRect
                                                     forTitle:[NSString stringWithFormat:TITLE_DELMSG, [PBUserInterfaceProvider getDataSizeString:_totalSpaceFreed]]
                                                   alignement:NSTextAlignmentCenter
                                                     tagValeu:tag++];
        
    
    yCoordinate += MINIMUM_SPACING * 2;
    NSArray *allRootPathKey = [_deletedFilePropertiesDictionary allKeys];
    [contentView addSubview:statTitelTextView];
    
    [PBUserInterfaceProvider setAutoLayoutConstraintXYCenterBottomInView:_scrollView toView:dataTextField];
    [PBUserInterfaceProvider setAutoLayoutConstraintXYCenterBottomInView:_scrollView toView:statTitelTextView];
    for(NSString *path in allRootPathKey) {
        NSMutableArray *deleteProArr = [_deletedFilePropertiesDictionary objectForKey:path];
        long long dataDeleted = 0;
        long deleteCount = 0;
        for(PBDeleteProperties *delPro in deleteProArr) {
            dataDeleted += delPro.fileDataLength;
            deleteCount++;
        }
        NSView *stateView = [[NSView alloc] initWithFrame:CGRectMake(MINIMUM_SPACING, yCoordinate, 400, 200)];
        titleTextRect = CGRectMake(MINIMUM_SPACING, MINIMUM_SPACING, 130, MINIMUM_SPACING);
        CGRect descTextRect = CGRectMake(150, MINIMUM_SPACING, 250, MINIMUM_SPACING);
        NSTextField *titelTextView = [PBUserInterfaceProvider getTextFiledWith:titleTextRect
                                                                      forTitle:TITLE_ROOTURL
                                                                    alignement:NSTextAlignmentLeft
                                                                      tagValeu:tag++];
        NSTextField *descTextView = [PBUserInterfaceProvider getTextFiledWith:descTextRect
                                                                       forTitle:path
                                                                     alignement:NSTextAlignmentLeft
                                                                       tagValeu:tag++];
        titleTextRect = CGRectMake(MINIMUM_SPACING, MINIMUM_SPACING, 130, MINIMUM_SPACING);
        descTextRect = CGRectMake(150, MINIMUM_SPACING, 250, MINIMUM_SPACING);
        titelTextView = [PBUserInterfaceProvider getTextFiledWith:titleTextRect
                                                         forTitle:TITLE_FREESPACE
                                                       alignement:NSTextAlignmentLeft
                                                         tagValeu:tag++];
        descTextView = [PBUserInterfaceProvider getTextFiledWith:descTextRect
                                                        forTitle:[PBUserInterfaceProvider getDataSizeString:dataDeleted]
                                                      alignement:NSTextAlignmentLeft
                                                        tagValeu:tag++];
        [stateView addSubview:titelTextView];
        [stateView addSubview:descTextView];
        titleTextRect = CGRectMake(MINIMUM_SPACING, 60, 130, MINIMUM_SPACING);
        descTextRect = CGRectMake(150, 60, 250, MINIMUM_SPACING);
        titelTextView = [PBUserInterfaceProvider getTextFiledWith:titleTextRect
                                                         forTitle:TITLE_IMGCOUNT
                                                       alignement:NSTextAlignmentLeft
                                                         tagValeu:tag++];
        descTextView = [PBUserInterfaceProvider getTextFiledWith:descTextRect
                                                        forTitle:[NSString stringWithFormat:FORMAT_COUNT, deleteCount]
                                                      alignement:NSTextAlignmentLeft
                                                        tagValeu:tag++];
        [stateView addSubview:titelTextView];
        [stateView addSubview:descTextView];
        
        titleTextRect = CGRectMake(MINIMUM_SPACING, 100, 130, MINIMUM_SPACING);
        descTextRect = CGRectMake(150, 100, 250, MINIMUM_SPACING);
        titelTextView = [PBUserInterfaceProvider getTextFiledWith:titleTextRect
                                                                      forTitle:TITLE_ROOTURL
                                                                    alignement:NSTextAlignmentLeft
                                                                      tagValeu:tag++];
        descTextView = [PBUserInterfaceProvider getTextFiledWith:descTextRect
                                                                     forTitle:path
                                                                   alignement:NSTextAlignmentLeft
                                                                     tagValeu:tag++];
        [stateView addSubview:titelTextView];
        [stateView addSubview:descTextView];
        [contentView addSubview:stateView];
        yCoordinate += 220;
    }
        _scrollView.documentView.frame = NSMakeRect(0, 0,statisctisView.bounds.size.width, yCoordinate);
}
@end

@implementation PBDeleteProperties

-(instancetype)initWithProperties:(PBFileProperties *)fileProperties {
    self = [super init];
    if(self) {
        _isDuplicate = fileProperties.isDuplicate;
        _fileRootURL = [fileProperties.fileUrl URLByDeletingLastPathComponent];
        _fileName = fileProperties.fileName;
        _fileDataLength = fileProperties.fileDataLength;
        _deletionDateTime = [NSDate date];
    }
    return self;
}

@end
