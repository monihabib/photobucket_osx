//
//  PBDeleteManager.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 12/28/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBFileProperties.h"
#import "PBUserInterfaceProvider.h"


@interface PBDeleteManager : NSObject
{
    BOOL isRootDirAlreadyExist;
    NSString *currentRootDirectory;
}

@property (readonly) NSMutableDictionary *deletedFilePropertiesDictionary;

@property (readonly) NSMutableArray *rootDirUrlArray;
@property (readonly) long long totalSpaceFreed;
@property (readonly) long numberOfFilesDeleted;
@property (readwrite) NSScrollView *scrollView;

//@property (readonly) 
-(void)updateDeletionStatistics:(PBFileProperties *)fileProperties;
-(void)setRootUrl:(NSString *)rootDirPath;
-(void)performDeletionStatistics:(NSView *)statisctisView;
@end





@interface PBDeleteProperties : NSObject
@property (readonly) NSString *fileName;
@property (readonly) NSURL *fileRootURL;
@property (readonly) BOOL isDuplicate;
@property (readonly) NSDate *deletionDateTime;
@property (readonly) long fileDataLength;
//@property (readonly)

-(instancetype)initWithProperties:(PBFileProperties *)fileProperties;

@end
