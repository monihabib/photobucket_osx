//
//  PBAlgorithms.m
//  SameImageCutter
//
//  Created by Mostafizur Rahman on 10/18/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBAlgorithms.h"

@implementation PBAlgorithms
+(int)getSearchIndex:(NSMutableArray *)arrayToUpdate withData:(long)fileDataLength
{
    int startIndex = 0;
    int endIndex = (int)arrayToUpdate.count - 1;
    int middleIndex = (startIndex + endIndex) / 2;
    
    while (startIndex <= endIndex)
    {
        PBFileProperties *imageFile = [arrayToUpdate objectAtIndex:middleIndex];
        if (imageFile.fileDataLength < fileDataLength) {
            startIndex = middleIndex + 1;
        } else if (imageFile.fileDataLength == fileDataLength) {
            return middleIndex;
        } else {
            endIndex = middleIndex - 1;
        }
        middleIndex = (startIndex + endIndex)/2;
    }
    
    return startIndex;
    
}
@end
