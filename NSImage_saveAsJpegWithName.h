//
//  NSImage_saveAsJpegWithName.h
//  ImageBucket
//
//  Created by Mostafizur Rahman on 11/11/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSImage(saveAsJpegWithName)
- (BOOL) saveAsJpegWithName:(NSString*) fileName;
-(BOOL)saveImageAsPng:(NSString *)path withImageRef:(CGImageRef)cgRef;
@end

@implementation NSImage(saveAsJpegWithName)

- (BOOL) saveAsJpegWithName:(NSString*) fileName
{
    // Cache the reduced image
    NSData *imageData = [self TIFFRepresentation];
    NSBitmapImageRep *imageRep = [NSBitmapImageRep imageRepWithData:imageData];
    NSDictionary *imageProps = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:1.0] forKey:NSImageCompressionFactor];
    imageData = [imageRep representationUsingType:NSJPEGFileType properties:imageProps];
    return [imageData writeToFile:fileName atomically:NO];
}
-(BOOL)saveImageAsPng:(NSString *)path withImageRef:(CGImageRef)cgRef {
    
    NSBitmapImageRep *newRep = [[NSBitmapImageRep alloc] initWithCGImage:cgRef];
    [newRep setSize:[self size]];   // if you want the same resolution
    NSDictionary *options = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:1.0] forKey:NSImageCompressionFactor];
    NSData *pngData = [newRep representationUsingType:NSPNGFileType properties:options];
    return [pngData writeToFile:path atomically:YES];
}
@end
