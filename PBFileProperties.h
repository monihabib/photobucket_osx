//
//  PBImageProperties.h
//  SameImageCutter
//
//  Created by Mostafizur Rahman on 10/17/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import "PBAlgorithms.h"
#import "PBDefines.h"
#import "NSImage_saveAsJpegWithName.h"

@interface PBFileProperties : NSObject
@property (readwrite) NSURL *fileUrl;

@property (readwrite) NSString *fileId;
@property (readwrite) NSString *fileName;
@property (readwrite) NSString *fileType;
@property (readwrite) NSDateComponents *fileCreationDate;
@property (readwrite) NSString *fileLastModified;
@property (readwrite) NSString *imageHashTag; // for search parpuse, this could be set during image cupture, from specific iOS camera app.
@property (readwrite) NSString *imageGPSPosition;
@property (readwrite) NSString *imageCameraModel;
@property (readwrite) NSString *softwareName;
@property (readwrite) NSString *copywriteName;
@property (readwrite) NSURL *imageWebsourceURL;
@property (readwrite) NSURL *thumbImageURL;
@property (readwrite) long imagePixelWidth;
@property (readwrite) long imagePixelHeight;
@property (readwrite) double fileDataLength;

@property (readwrite) BOOL isSelected;
@property (readwrite) BOOL isDeleted;
@property (readwrite) BOOL isMoved;
@property (readwrite) BOOL isDuplicate;
-(instancetype)initWithUrl:(NSURL *)imageUrl thumb:(NSURL *)thumbUrl;
//+(NSInteger)setImageListWithFileProperties:(PBFileProperties __autoreleasing*)imageFileProperties
//                       withImageArray:(NSMutableArray __autoreleasing*)rootImageArray;
@end
