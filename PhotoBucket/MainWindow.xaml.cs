﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Windows.Controls;
using System.ComponentModel;
using System.Threading;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhotoBucket
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Deletion Delegate
        public delegate void OnDeletionCompleted();
        OnDeletionCompleted deletionCompletionDelegate;

        private void resetInitialState(string title, string message)
        {
            isBackgroundWorkerBusy = false;
            directoryExtractor.isRootDirProccessed = false;

            MessageBoxButton buttons = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Asterisk;
            MessageBox.Show(message, title, buttons, icon);
        }


        private void deleteImageCompleted()
        {
            resetInitialState("Deletion completed!", "Selected files are deleted successfully. See statistics for more information...");
            deleteAllFiles.IsEnabled = true;
        }
        #endregion

        #region Sid Delegate
        public delegate void OnBadSidRemoveCompleted();
        OnBadSidRemoveCompleted badsidCompletionDelegate;
        private void badSidRemoveCompleted()
        {
            resetInitialState("Corrupted images proccessed.", "Bad sids are removed and current user is set as admin in selected directory.");
            badSidImageProcessing.IsEnabled = true;            
        }
        #endregion

        #region Move Delegate
        public delegate void OnFileMovingCompleted();
        OnFileMovingCompleted moveImageCompletionDelegate;
        private void moveImageCompleted()
        {
            resetInitialState("Moving completed!", "Selected files are moved to destination directory successfully. See statistics for more information...");
            moveAndGroupButton.IsEnabled = true;
        }
        #endregion


        private bool shouldDeleteImage = false;
        private bool shouldMoveImage = false;
        private readonly BackgroundWorker backgroundWorker = new BackgroundWorker();
        private readonly Dictionary<object,string > textBoxWithHintsDictionary = new Dictionary<object, string>();
        private ControlEventHandler eventHandler;
        public DirectoryExtractor directoryExtractor;
        private bool isBackgroundWorkerBusy;
        public MainWindow()
        {
            InitializeComponent();

            searchProcessStatusLabel.HorizontalContentAlignment = HorizontalAlignment.Center;
            fileReadingProgressBar.IsIndeterminate = false;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;      
            setTextBoxList();
            InitializeBackgroundWorker();
            badsidCompletionDelegate = new OnBadSidRemoveCompleted(badSidRemoveCompleted);
            deletionCompletionDelegate = new OnDeletionCompleted(deleteImageCompleted);
            moveImageCompletionDelegate = new OnFileMovingCompleted(moveImageCompleted);
            ArrayList parameterList = new ArrayList();
            parameterList.Add(progressStatusLabel);
            parameterList.Add(imageStatusBox);
            parameterList.Add(badsidCompletionDelegate);
            parameterList.Add(deletionCompletionDelegate);
            parameterList.Add(moveImageCompletionDelegate);
            directoryExtractor = new DirectoryExtractor(backgroundWorker, parameterList);

        }

        private void setTextBoxList()
        {            
            foreach (TextBox textBox in InterfaceProvider.FindLogicalChildren<TextBox>(this))
            {
                textBoxWithHintsDictionary.Add(textBox, textBox.Text);
            }
            eventHandler = new ControlEventHandler(textBoxWithHintsDictionary);
        }

        private void directoryChooserButton_Click(object sender, RoutedEventArgs e)
        {
            Button dirChooserButton = sender as Button;
            string hintsText = dirChooserButton.Tag.ToString();
            foreach (var textBoxItem in textBoxWithHintsDictionary)
            {
                if (textBoxItem.Value.Equals(hintsText))
                {
                    TextBox textBox = (TextBox)textBoxItem.Key;
                    eventHandler.openDirectoryChooser(textBox);
                    clearCounterLabelContent();
                    break;
                }
            }            
        }
        
        private void subDirectoryComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void subDirectoryComboBox_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void refreshRootDirImageButton_Click(object sender, RoutedEventArgs e)
        {
            if (shouldInvokeRequestAction(sender, e))
            {
                performAction(sender, e);
            }
        }

        

        private void badSidImageProcessing_Click(object sender, RoutedEventArgs e)
        {
            if(shouldInvokeRequestAction(sender, e))
            {
                string badSidPath = badSidDirectoryPath.Text;
                if (Directory.Exists(badSidPath))
                {
                    isBackgroundWorkerBusy = true;
                    badSidImageProcessing.IsEnabled = false;
                    ArrayList paramList = new ArrayList();
                    paramList.Add(badSidStatusLabel);
                    paramList.Add(badsidProgressBar);
                    paramList.Add(badSidStatisticsLabel);
                    directoryExtractor.removeBadSid(badSidPath, paramList);
                }
            }
        }
        
        private void InitializeBackgroundWorker()
        {
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
            backgroundWorker.WorkerReportsProgress = true;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            ArrayList inputParameteres = e.Argument as ArrayList;
            DirectoryExtractor dirExtractor = inputParameteres[0] as DirectoryExtractor;
            string path = inputParameteres[1] as string;
            dirExtractor.processRootDirectorySystem(path);
        }

        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else if (e.Cancelled)
            {
                searchProcessStatusLabel.Content = "Image searching aborted! ◕⁔◕";
            }
            else
            {
                searchProcessStatusLabel.Foreground = new SolidColorBrush(Color.FromArgb(255, 60, 170, 60));
                searchProcessStatusLabel.Content = "Image files are read successfully! ಠ‿ಠ";
            }
            initializeUserInterfaceAfterBackgroundProcess();
            if (shouldDeleteImage)
            {
                performActionDelete();
            }
            else if (shouldMoveImage)
            {
                performActionMove();
            }
        }
        
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.fileReadingProgressBar.Value = e.ProgressPercentage;
            if (e.ProgressPercentage == 1)
            {
                progressStatusLabel.Content = "Getting subdirectories in root folder...";
            }
            else if (e.ProgressPercentage == 2)
            {
                progressStatusLabel.Content = "Sorting and finding duplicate images...";
            }
            else if (e.ProgressPercentage == 3)
            {
                progressStatusLabel.Content = "setting up status for file output...";
            }
            else
            {

            }
        }

        private void initializeUserInterfaceBeforeBackgroundProcess()
        {
            searchProcessStatusLabel.Content = "";
            fileReadingProgressBar.IsIndeterminate = true;
            refreshRootDirImage.IsEnabled = false;
            progressStatusLabel.Content = "";
            clearCounterLabelContent();
        }

        private void clearCounterLabelContent()
        {
            duplicateImageCountLabel.Content = "";
            subDirectoryCountLabel.Content = "";
            totalImageCountLabel.Content = "";
            fileSizeLabel.Content = "";
        }

        private void initializeUserInterfaceAfterBackgroundProcess()
        {
            fileReadingProgressBar.IsIndeterminate = false;
            fileReadingProgressBar.Value = 0;
            refreshRootDirImage.IsEnabled = true;
            progressStatusLabel.Content = "";
            isBackgroundWorkerBusy = false;
        }



        private bool shouldInvokeRequestAction(object sender, RoutedEventArgs e)
        {
            if (isBackgroundWorkerBusy)
            {
                MessageBox.Show("Root directory path handling other operations.\nPlease! wait patiently...", "Background Worker Busy!");
                return false;
            }
            if (sender.Equals(badSidImageProcessing))
            {
                if (badSidDirectoryPath.Text.Equals("") || badSidDirectoryPath.Text.Equals("Enter folder path containing corrupted images..."))
                {
                    MessageBox.Show("Input directory is not set. Choose a directory containing inaccessible images and procced.", "Sid directoyr empty!");
                    return false;
                }
                return true;
            }

            if (sender.Equals(moveAndGroupButton))
            {
                if (moveDestinationTextBox.Text.Equals("") || moveDestinationTextBox.Text.Equals("Moving Destination Directory"))
                {
                    MessageBox.Show("Destination directory is not set. Choose a moving destination directory and procced.", "Destination directoyr empty!");
                    return false;
                }
            }
            if (rootDirectoryTextBox.Text.Equals("") ||
                rootDirectoryTextBox.Text.Equals("Choose root image folder path"))
            {
                MessageBox.Show("Root directory is not set. Choose a root directory.", "Root directoyr Empty!");
                clearCounterLabelContent();
                return false;
            }



            return true;
        }

        private void rootDirectoryTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (shouldInvokeRequestAction(sender, e))
                {
                    performAction(rootDirectoryTextBox, null);
                }
            }
        }

        private void performAction(object sender, RoutedEventArgs e)
        {
            
                string rootDirPath = rootDirectoryTextBox.Text;
                if (Directory.Exists(rootDirPath))
                {
                    isBackgroundWorkerBusy = true;
                    initializeUserInterfaceBeforeBackgroundProcess();
                    ArrayList parameterList = new ArrayList();
                    parameterList.Add(directoryExtractor);
                    parameterList.Add(rootDirectoryTextBox.Text);
                    backgroundWorker.RunWorkerAsync(parameterList);
                }
                else
                {
                    searchProcessStatusLabel.Foreground = new System.Windows.Media.SolidColorBrush(Color.FromArgb(255, 200, 80, 80));
                    clearCounterLabelContent();
                    searchProcessStatusLabel.Content = "Image searching aborted! ◕⁔◕";
                    MessageBox.Show("Root directory folder does not exists. Invalid path : " + rootDirPath, "Directory Path invalid...");
                }
            
        }

        private void deleteAllFiles_Click(object sender, RoutedEventArgs e)
        {
            if (shouldInvokeRequestAction(sender, e))
            {
                if (!directoryExtractor.isRootDirProccessed)
                {
                    performAction(rootDirectoryTextBox, null);
                    shouldDeleteImage = true;
                }
                else
                {
                    performActionDelete();
                }
            }
        }

        private void performActionDelete()
        {
            shouldDeleteImage = false;
            isBackgroundWorkerBusy = true;
            ArrayList filterParamArrayList = getDeletionFilterArrayList();
            directoryExtractor.deleteImages(filterParamArrayList);
        }

        private void performActionMove()
        {
            shouldMoveImage = false;
            isBackgroundWorkerBusy = true;
            ArrayList filterParamArrayList = getMovingFilterArrayList();
            directoryExtractor.moveFiles(filterParamArrayList);
        }

        private ArrayList getDeletionFilterArrayList()
        {
            ArrayList filterParamArrayList = new ArrayList();
            filterParamArrayList.Add(deletionStatusLabel);
            filterParamArrayList.Add(deletionProgressBar);
            return filterParamArrayList;

        }

        private ArrayList getMovingFilterArrayList()
        {
            ArrayList filterParamArrayList = new ArrayList();
            filterParamArrayList.Add(movingProgressBar);
            filterParamArrayList.Add(moveDestinationTextBox.Text);
            int selectedIndex = comboboxGroupingOption.SelectedIndex;
            filterParamArrayList.Add(selectedIndex);
            return filterParamArrayList;

        }

        

        private void moveAndGroupButton_Click(object sender, RoutedEventArgs e)
        {
            if (shouldInvokeRequestAction(sender, e))
            {
                if (!directoryExtractor.isRootDirProccessed)
                {
                    performAction(rootDirectoryTextBox, null);
                    shouldMoveImage = true;
                }
                else
                {
                    performActionMove();
                }
            }
        }
    }
}
