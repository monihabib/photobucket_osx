﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace PhotoBucket
{
    class ControlEventHandler
    {


        private readonly Dictionary<object, string> textBoxWithHintsDictionary = new Dictionary<object, string>();
        public ControlEventHandler(Dictionary<object, string> dictionary)
        {
            textBoxWithHintsDictionary = dictionary;
            foreach (var item in dictionary)
            {
                TextBox textBox = (TextBox)item.Key;
                setEventHandler(textBox);
            }

        }

        public void setEventHandler(TextBox textBox)
        {
            textBox.GotFocus += new System.Windows.RoutedEventHandler(TextBoxGetFocused);
            textBox.LostFocus += new System.Windows.RoutedEventHandler(TextBoxLostFocus);
            textBox.MouseUp += new System.Windows.Input.MouseButtonEventHandler(TextBoxMouseUp);
            textBox.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(TextBoxMouseUp);
        }

        public void TextBoxLostFocus(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            bool shouldHintsTextShow = textBox.Text == "";
            textBox.Text = shouldHintsTextShow ? textBoxWithHintsDictionary[textBox] : textBox.Text;
            textBox.Foreground = new SolidColorBrush(shouldHintsTextShow ? Color.FromArgb(0xFF, 0xAA, 0xAA, 0xAA) : Color.FromArgb(0xFF, 0x50, 0x50, 0x50));
            textBox.FontStyle = shouldHintsTextShow ? FontStyles.Italic : FontStyles.Normal;
            
        }

        public void TextBoxGetFocused(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;

            bool shouldHintsTextHide = textBoxWithHintsDictionary[textBox].Equals(textBox.Text);
            textBox.Foreground = new SolidColorBrush(Color.FromArgb(0xFF, 0x50, 0x50, 0x50));
            textBox.FontStyle = FontStyles.Normal;
            if (shouldHintsTextHide)
            {
                textBox.Text = "";
            }
        }

        public void TextBoxMouseUp(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            textBox.SelectAll();
        }

        public void openDirectoryChooser(TextBox textBox)
        {
            var folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowserDialog.ShowNewFolderButton = false;
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox.Text = folderBrowserDialog.SelectedPath;
                TextBoxGetFocused(textBox, null);
            }
        }
    }
}
