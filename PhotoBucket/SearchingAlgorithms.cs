﻿
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace PhotoBucket
{
    class SearchingAlgorithms
    {
        public static int getSearchIndex(List<FileInfo> arrayToUpdate, long fileDataLength)
        {
            int startIndex = 0;
            int endIndex = (int)arrayToUpdate.Count - 1;
            int middleIndex = (startIndex + endIndex) / 2;
            while (startIndex <= endIndex)
            {
                FileInfo imageFile = arrayToUpdate.ElementAt(middleIndex);
                if (imageFile.Length < fileDataLength)
                {
                    startIndex = middleIndex + 1;
                }
                else if (imageFile.Length == fileDataLength)
                {
                    return middleIndex;
                }
                else
                {
                    endIndex = middleIndex - 1;
                }
                middleIndex = (startIndex + endIndex) / 2;
            }
            return startIndex;
        }
    }
}
