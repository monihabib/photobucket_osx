﻿

using System;

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Windows.Media.Imaging;

namespace PhotoBucket
{
    public class DirectoryExtractor
    {
        private List<SID_INFO> sidListers;
        private delegate void OnSidListUpdated(SID_INFO sidInfo);
        private void updateSidList(SID_INFO sidInfo)
        {
            sidListers.Add(sidInfo);
            (uiupdaterList[2] as MainWindow.OnBadSidRemoveCompleted)();
        }


        public bool isRootDirProccessed { get; set; }
        public string rootDirectoryPath { get; private set; }
        public List<FileInfo> imageFileList { get; private set; }
        public long fileRead { get; private set; }
        public long duplicateFileCount { get; private set; }
        public long deletedFileSize { get; private set; }
        public Dictionary<long, List<FileInfo>> imageFilesLenDictionary;
        private DirectoryInfo rootDirectoryInfo;
        private List<DirectoryInfo> subDirectoryList;
        private readonly BackgroundWorker backgroundWorker;
        private readonly ArrayList uiupdaterList;
        string fileSizeString = "";
        public DirectoryExtractor(BackgroundWorker bgWorker, ArrayList uilist)
        {
            sidListers = new List<SID_INFO>();
            backgroundWorker = bgWorker;
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
            uiupdaterList = uilist;
            isRootDirProccessed = false;

        }
        public DirectoryExtractor(string path)
        {
            setRootDirectory(path);
        }

        public void setRootDirectory(string rootDirPath)
        {

            if (subDirectoryList != null)
            {
                subDirectoryList.Clear();
            }
            isRootDirProccessed = false;
            rootDirectoryPath = rootDirPath;
            rootDirectoryInfo = new DirectoryInfo(rootDirectoryPath);
        }

        public void processRootDirectorySystem(string rootDirectoryPath)
        {
            setRootDirectory(rootDirectoryPath);
            backgroundWorker.ReportProgress(1);
            subDirectoryList = BadSidHandler.getBadSidDirectoryList(rootDirectoryPath);
            List<FileInfo> unsortedImageFileList = new List<FileInfo>();
            foreach (DirectoryInfo dirInfo in subDirectoryList)
            {
                try
                {
                    unsortedImageFileList.AddRange(dirInfo.GetFiles());
                }
                catch
                {
                    MessageBox.Show("Access denied at file system. Directory Path : " + dirInfo.FullName, "Access Denied To Set Permission");
                }
            }
            backgroundWorker.ReportProgress(2);
            sortImageFilesToFindDuplicateImages(unsortedImageFileList);
            isRootDirProccessed = true;
        }

        private void sortImageFilesToFindDuplicateImages(List<FileInfo> unsortedImageFileList)
        {
            imageFileList = new List<FileInfo>();
            imageFilesLenDictionary = new Dictionary<long, List<FileInfo>>();
            fileSizeString = "";
            fileRead = 0;
            duplicateFileCount = 0;
            foreach (FileInfo randomFileInfo in unsortedImageFileList)
            {
                fileRead += randomFileInfo.Length;
                int positionIndex = SearchingAlgorithms.getSearchIndex(imageFileList, randomFileInfo.Length);
                imageFileList.Insert(positionIndex, randomFileInfo);
                int fileCount = imageFileList.Count;

                List<FileInfo> duplicateImageList = null;
                if (imageFilesLenDictionary.ContainsKey(randomFileInfo.Length))
                {
                    duplicateImageList = imageFilesLenDictionary[randomFileInfo.Length] as List<FileInfo>;
                }
                if (duplicateImageList == null)
                {
                    duplicateImageList = new List<FileInfo>();
                    duplicateImageList.Add(randomFileInfo);
                    imageFilesLenDictionary.Add(randomFileInfo.Length, duplicateImageList);
                }
                else
                {
                    duplicateFileCount++;
                    duplicateImageList.Add(randomFileInfo);
                }
            }
            //foreach (var obj in imageFilesLenDictionary)
            //{

            //    List<FileInfo> imageFileList = obj.Value as List<FileInfo>;
            //    int count = imageFileList.Count;
            //    if (count > 1)
            //    {
            //        foreach (FileInfo fileInfo in imageFileList)
            //        {
            //            Console.WriteLine("Key {0} count {1}", obj.Key, fileInfo.FullName);
            //        }
            //        Console.WriteLine("_______________________________\n");
            //    }
            //}

            backgroundWorker.ReportProgress(3);
            setReadFileSize(fileRead);
        }

        private void setReadFileSize(long fileSize)
        {
            string[] sizeString = { " KB", " MB", " GB" };
            for (int i = 0; fileSize > 1024; i++)
            {
                fileSize = fileSize / 1024;
                fileSizeString = fileSize + sizeString[i];
            }
            backgroundWorker.ReportProgress(4);
        }





        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Label progressStatusLabel = uiupdaterList[0] as Label;
            if (e.ProgressPercentage == 1)
            {

                progressStatusLabel.Content = "Getting subdirectories in root folder...";
            }
            else if (e.ProgressPercentage == 2)
            {
                progressStatusLabel.Content = "Sorting and finding duplicate images...";
            }
            else if (e.ProgressPercentage == 3)
            {
                progressStatusLabel.Content = "setting up status for file output...";
            }
            else
            {
                GroupBox statusBox = uiupdaterList[1] as GroupBox;

                Dictionary<String, object> labelDictionary = new Dictionary<string, object>();
                labelDictionary.Add("subDirectoryCountLabel", subDirectoryList.Count);
                labelDictionary.Add("fileSizeLabel", fileSizeString);
                labelDictionary.Add("totalImageCountLabel", imageFileList.Count);
                labelDictionary.Add("duplicateImageCountLabel", duplicateFileCount);
                foreach (Label label in InterfaceProvider.FindLogicalChildren<Label>(statusBox))
                {
                    string key = label.Name.ToString().Trim();
                    if (labelDictionary.ContainsKey(key))
                    {
                        label.Content = labelDictionary[key].ToString();
                    }
                }
            }
        }





        #region Deletion Proccess
        private Label deletionStatusLabel;
        private ProgressBar deletionPrograss;

        public void deleteImages(ArrayList filterParamList)
        {
            deletionStatusLabel = filterParamList[0] as Label;
            deletionPrograss = filterParamList[1] as ProgressBar;


            BackgroundWorker deletionBackgroundWorker = new BackgroundWorker() { WorkerReportsProgress = true };
            deletionBackgroundWorker.DoWork += new DoWorkEventHandler(deletionBackgroundWorker_DoWork);
            deletionBackgroundWorker.ProgressChanged += new ProgressChangedEventHandler(deletionBackgroundWorker_ProgressChanged);
            deletionBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(deletionBackgroundWorker_RunWorkerCompleted);
            filterParamList.Add(imageFilesLenDictionary);
            deletedFileSize = 0;
            filterParamList.Add(deletedFileSize);
            deletionBackgroundWorker.RunWorkerAsync(filterParamList);
        }

        private void deletionBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ArrayList paramList = e.Argument as ArrayList;
            Dictionary<long, List<FileInfo>> sizeFilesDictionary = paramList[2] as Dictionary<long, List<FileInfo>>;
            long deleteFileSize = (long)paramList[3];
            BackgroundWorker deleteWorker = sender as BackgroundWorker;
            bool yesToAll = false;
            long fileDeleted = 0;
            foreach (var keyValue in sizeFilesDictionary)
            {
                List<FileInfo> fileList = keyValue.Value as List<FileInfo>;
                if (fileList.Count > 1)
                {
                    long fileSize = keyValue.Key;
                    long fileCount = fileList.Count;
                    FileInfo orginalFileInfo = fileList[0];
                    for (int index = 1; index < fileCount; index++)
                    {
                        FileInfo fileInfo = fileList[1];
                        if (!fileInfo.LastWriteTime.Equals(orginalFileInfo.LastWriteTime)) continue;

                        deleteFileSize += fileInfo.Length;
                        try
                        {
                            fileList.Remove(fileInfo);
                            File.Delete(fileInfo.FullName);
                        }
                        catch (Exception ex)
                        {
                            if (!yesToAll)
                            {
                                string message = "Deletion failed! ◕⁔◕\n" + ex.Message + "\npath : " + fileInfo.FullName + "\n\nStop further message popup and proceed to next?";
                                string caption = "Unable to delete file";
                                MessageBoxButton buttons = MessageBoxButton.YesNo;
                                MessageBoxImage icon = MessageBoxImage.Question;
                                yesToAll = MessageBox.Show(message, caption, buttons, icon) == MessageBoxResult.Yes;
                            }

                        }
                        fileDeleted++;
                        int progress = (int)(fileDeleted * 100 / duplicateFileCount);
                        deleteWorker.ReportProgress(progress, fileInfo.FullName);
                    }
                }
            }
        }

        //public static bool ShowDialog(string text, string caption)
        //{
        //    System.Windows.Form prompt = new System.Windows.Form();
        //    prompt.Width = 180;
        //    prompt.Height = 100;
        //    prompt.Text = caption;
        //    FlowLayoutPanel panel = new FlowLayoutPanel();
        //    CheckBox chk = new CheckBox();
        //    chk.Text = text;
        //    Button ok = new Button() { Text = "Yes" };
        //    ok.Click += (sender, e) => { prompt.Close(); };
        //    Button no = new Button() { Text = "No" };
        //    no.Click += (sender, e) => { prompt.Close(); };
        //    panel.Controls.Add(chk);
        //    panel.SetFlowBreak(chk, true);
        //    panel.Controls.Add(ok);
        //    panel.Controls.Add(no);
        //    prompt.Controls.Add(panel);
        //    prompt.ShowDialog();
        //    return chk.Checked;
        //}

        private void deletionBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs progressArgs)
        {
            deletionStatusLabel.Content = "deleted : " + progressArgs.UserState.ToString();
            deletionPrograss.Value = progressArgs.ProgressPercentage;
        }

        private void deletionBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs completedArgs)
        {
            deletionPrograss.IsIndeterminate = false;
            deletionStatusLabel.Content = "Deletion completed. " + (deletedFileSize / 1024 / 1024) + " MB " + duplicateFileCount + " duplicate image(s) deleted.";
            BackgroundWorker deletionBackgroundWorker = sender as BackgroundWorker;
            deletionBackgroundWorker.DoWork -= new DoWorkEventHandler(deletionBackgroundWorker_DoWork);
            deletionBackgroundWorker.ProgressChanged -= new ProgressChangedEventHandler(deletionBackgroundWorker_ProgressChanged);
            deletionBackgroundWorker.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(deletionBackgroundWorker_RunWorkerCompleted);
            deletionBackgroundWorker.Dispose();
            (uiupdaterList[3] as MainWindow.OnDeletionCompleted)();
        }
        #endregion



        #region Move files
        private ProgressBar movingPrograss;
        public void moveFiles(ArrayList optionArrayList)
        {
            movingPrograss = optionArrayList[0] as ProgressBar;
            BackgroundWorker moveBackgroundWorker = new BackgroundWorker() { WorkerReportsProgress = true };
            moveBackgroundWorker.DoWork += new DoWorkEventHandler(moveBackgroundWorker_DoWork);
            moveBackgroundWorker.ProgressChanged += new ProgressChangedEventHandler(moveBackgroundWorker_ProgressChanged);
            moveBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(moveBackgroundWorker_RunWorkerCompleted);

            optionArrayList.Add(rootDirectoryPath);
            optionArrayList.Add(subDirectoryList);
            optionArrayList.Add(imageFileList);
            moveBackgroundWorker.RunWorkerAsync(optionArrayList);
        }



        private void moveBackgroundWorker_DoWork(object sender, DoWorkEventArgs eventArgs)
        {
            BackgroundWorker moveBackgroundWorker = sender as BackgroundWorker;
            ArrayList paramList = eventArgs.Argument as ArrayList;
            string rootPath = paramList[3] as string;
            List<DirectoryInfo> subDirList = paramList[4] as List<DirectoryInfo>;
            List<FileInfo> imageList = paramList[5] as List<FileInfo>;
            string destinationPath = paramList[1] as string + "\\___Image_Bucket____";
            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }
            int type = (int)(paramList[2]); // comment type. higherarchy, date time creation.
            switch (type)
            {
                case 0:
                    moveAndGroupedByFolderHierarchy(destinationPath, subDirList, rootPath, moveBackgroundWorker, imageList);
                    break;
                case 1:
                    moveAndGroupedByComments(destinationPath, subDirList, rootPath, moveBackgroundWorker, imageList);
                    break;
                case 2:
                    moveAndGroupedByDateTime(destinationPath, subDirList, rootPath, moveBackgroundWorker, imageList);
                    break;
                default:
                    break;
            }


            // move type root higherarchy

        }

        private void moveAndGroupedByFolderHierarchy(string destinationPath, List<DirectoryInfo> subDirList,
            string rootPath, BackgroundWorker moveBackgroundWorker, List<FileInfo> imageList)
        {
            bool shouldContinue = false;
            moveBackgroundWorker.ReportProgress(-1);
            long count = subDirList.Count;
            List<string> lastPathCompList = new List<string>();
            for (int index = 0; index < count; index++)
            {
                DirectoryInfo dirInfo = subDirList[index];

                string lastPathComponent = dirInfo.FullName.Remove(0, rootPath.Length);
                lastPathCompList.Add(lastPathComponent);
                string destPath = destinationPath + lastPathComponent;
                createPath(destPath);
            }

            moveBackgroundWorker.ReportProgress(-2);
            count = imageList.Count;
            for (long index = 0; index < count; index++)
            {
                FileInfo fileInfo = imageList[(int)index];


                string imageDirectoryPath = fileInfo.DirectoryName.Remove(0, rootPath.Length);
                string filePath = destinationPath + imageDirectoryPath + "\\" + fileInfo.Name;
                try
                {
                    if (!File.Exists(filePath))
                    {
                        fileInfo.MoveTo(filePath);
                    }
                    else
                    {
                        do
                        {
                            try
                            {
                                filePath = destinationPath
                                    + imageDirectoryPath
                                    + "\\" + Path.GetFileNameWithoutExtension(fileInfo.Name)
                                    + getRandomString(6)
                                    + fileInfo.Extension;
                                fileInfo.MoveTo(filePath);
                            }
                            catch (IOException ioex)
                            {
                                if (!shouldContinue)
                                {
                                    string message = "Move failed!!! ◕⁔◕\n" + ioex.Message + "\npath : " + fileInfo.FullName + "\n\nStop further message popup and proceed to next?";
                                    string caption = "Unable to delete file";
                                    MessageBoxButton buttons = MessageBoxButton.YesNo;
                                    MessageBoxImage icon = MessageBoxImage.Question;
                                    shouldContinue = MessageBox.Show(message, caption, buttons, icon) == MessageBoxResult.Yes;
                                    if (ioex.Message.Contains(BucketConstant.RESOURCE_IS_BEING_USED))
                                    {
                                        break;
                                    }
                                }
                            }
                        } while (!File.Exists(filePath));
                    }
                    int percent = (int)(index * 100 / count);
                    moveBackgroundWorker.ReportProgress(percent);
                }
                catch (IOException ioex)
                {
                    if (!shouldContinue)
                    {
                        string message = "Move failed!!! ◕⁔◕\n" + ioex.Message + "\npath : " + fileInfo.FullName + "\n\nStop further message popup and proceed to next?";
                        string caption = "Unable to delete file";
                        MessageBoxButton buttons = MessageBoxButton.YesNo;
                        MessageBoxImage icon = MessageBoxImage.Question;
                        MessageBoxResult result = MessageBox.Show(message, caption, buttons, icon);
                        shouldContinue = result == MessageBoxResult.Yes;


                    }
                }
            }
            moveBackgroundWorker.ReportProgress(100);
        }

        private void moveAndGroupedByComments(string destinationPath, List<DirectoryInfo> subDirList,
            string rootPath, BackgroundWorker moveBackgroundWorker, List<FileInfo> imageList)
        {

            bool shouldContinue = false;
            ArrayList createdFolderPathList = new ArrayList();
            long count = imageList.Count;
            for (long index = 0; index < count; index++)
            {
                FileInfo fileInfo = imageList[(int)index];
                string comment = "no_comments";
                using (FileStream fs = new FileStream(fileInfo.FullName, FileMode.Open))
                {
                    BitmapFrame bmpFrame = BitmapFrame.Create(fs);
                    BitmapMetadata bmpImageMetadata = (BitmapMetadata)bmpFrame.Metadata;
                    try
                    {
                        comment = bmpImageMetadata.Comment;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    fs.Close();
                }




                string directoryPath = destinationPath + "\\" + comment;
                string filePath = directoryPath + "\\" + fileInfo.Name;
                if (!createdFolderPathList.Contains(comment))
                {
                    createPath(directoryPath);
                    createdFolderPathList.Add(comment);
                }
                if (!File.Exists(filePath))
                {
                    fileInfo.MoveTo(filePath);
                }
                else
                {
                    do
                    {
                        try
                        {
                            filePath = directoryPath
                                    + Path.GetFileNameWithoutExtension(fileInfo.Name)
                                    + getRandomString(6)
                                    + fileInfo.Extension;

                            fileInfo.MoveTo(filePath);
                        }
                        catch (IOException ioex)
                        {
                            if (!shouldContinue)
                            {
                                string message = "Move failed!!! ◕⁔◕\n" + ioex.Message + "\npath : " + fileInfo.FullName + "\n\nStop further message popup and proceed to next?";
                                string caption = "Unable to delete file";
                                MessageBoxButton buttons = MessageBoxButton.YesNo;
                                MessageBoxImage icon = MessageBoxImage.Question;
                                shouldContinue = MessageBox.Show(message, caption, buttons, icon) == MessageBoxResult.Yes;
                                if (ioex.Message.Contains(BucketConstant.RESOURCE_IS_BEING_USED))
                                {
                                    break;
                                }
                            }
                        }
                    } while (!File.Exists(filePath));
                }
                int percent = (int)(index * 100 / count);
                moveBackgroundWorker.ReportProgress(percent);
            }
            moveBackgroundWorker.ReportProgress(100);
        }


        private object QueryMetadata(string query, BitmapMetadata bmpImageMetadata)
        {
            if (bmpImageMetadata.ContainsQuery(query))
                return bmpImageMetadata.GetQuery(query);
            else
                return null;
        }

        private void moveAndGroupedByDateTime(string destinationPath, List<DirectoryInfo> subDirList,
            string rootPath, BackgroundWorker moveBackgroundWorker, List<FileInfo> imageList)
        {
            ArrayList createdFolderPathList = new ArrayList();
            long count = imageList.Count;
            bool shouldContinue = false;
            for (long index = 0; index < count; index++)
            {
                FileInfo fileInfo = imageList[(int)index];
                DateTime imageTakenDate = new DateTime();
                try
                {
                    using (FileStream fs = new FileStream(fileInfo.FullName, FileMode.Open))
                    {

                        BitmapFrame bmpFrame = BitmapFrame.Create(fs);
                        BitmapMetadata bmpImageMetadata = (BitmapMetadata)bmpFrame.Metadata;

                        if (bmpImageMetadata.ContainsQuery("/app1/ifd/exif/subifd:{uint=36867}"))
                        {
                            string date = (string)bmpImageMetadata.GetQuery("/app1/ifd/exif/subifd:{uint=36867}");
                            imageTakenDate = new DateTime(
                                    int.Parse(date.Substring(0, 4)),    // year
                                    int.Parse(date.Substring(5, 2)),    // month
                                    int.Parse(date.Substring(8, 2)),    // day
                                    int.Parse(date.Substring(11, 2)),   // hour
                                    int.Parse(date.Substring(14, 2)),   // minute
                                    int.Parse(date.Substring(17, 2))    // second
                                );
                        }
                        else
                        {
                            imageTakenDate = fileInfo.LastWriteTime;
                        }

                        fs.Close();

                    }
                }
                catch
                {
                    imageTakenDate = fileInfo.LastWriteTime;
                }
                string directoryPath = createDateTimeFolderPath(imageTakenDate, destinationPath, createdFolderPathList);
                string filePath = directoryPath + "\\" + fileInfo.Name;
                if (!File.Exists(filePath))
                {
                    fileInfo.MoveTo(filePath);
                }
                else
                {
                    do
                    {
                        try
                        {
                            filePath = directoryPath + "\\"
                                    + Path.GetFileNameWithoutExtension(fileInfo.Name)
                                    + getRandomString(6)
                                    + fileInfo.Extension;
                            // filePath = directoryPath + "\\" + fileInfo.Name + getRandomString(6);
                            fileInfo.MoveTo(filePath);
                        }
                        catch (IOException ioex)
                        {
                            if (!shouldContinue)
                            {
                                string message = "Move failed!!! ◕⁔◕\n" + ioex.Message + "\npath : " + fileInfo.FullName + "\n\nStop further message popup and proceed to next?";
                                string caption = "Unable to delete file";
                                MessageBoxButton buttons = MessageBoxButton.YesNo;
                                MessageBoxImage icon = MessageBoxImage.Question;
                                shouldContinue = MessageBox.Show(message, caption, buttons, icon) == MessageBoxResult.Yes;
                                if (ioex.Message.Contains(BucketConstant.RESOURCE_IS_BEING_USED))
                                {
                                    break;
                                }
                            }
                        }
                    } while (!File.Exists(filePath));
                }
                int percent = (int)(index * 100 / count);
                moveBackgroundWorker.ReportProgress(percent);
            }
            moveBackgroundWorker.ReportProgress(100);
        }

        private string createDateTimeFolderPath(DateTime writeTime, string destinationPath, ArrayList dirList)
        {
            string path = destinationPath + "\\" + writeTime.Year.ToString()
                                            + "\\" + BucketConstant.MONTH_LIST[writeTime.Month - 1]
                                            + "\\" + writeTime.Date.ToLongDateString();
            if (!dirList.Contains(path))
            {
                dirList.Add(path);
                createPath(path);
            }
            return path;
        }


        private void createPath(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        private string getRandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length + 1];
            stringChars[0] = '_';
            var random = new Random();

            for (int i = 1; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            string fileName = new string(stringChars);
            return fileName;
        }

        private void moveBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs eventArgs)
        {
            int percent = eventArgs.ProgressPercentage; ;
            if (percent == -1)
            {
                movingPrograss.IsIndeterminate = true;
            }
            else if (percent == -2)
            {
                movingPrograss.IsIndeterminate = false;
            }
            else
            {
                movingPrograss.Value = percent;
            }
        }

        private void moveBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs completedArgs)
        {
            BackgroundWorker moveBackgroundWorker = sender as BackgroundWorker;
            moveBackgroundWorker.DoWork -= new DoWorkEventHandler(deletionBackgroundWorker_DoWork);
            moveBackgroundWorker.ProgressChanged -= new ProgressChangedEventHandler(deletionBackgroundWorker_ProgressChanged);
            moveBackgroundWorker.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(deletionBackgroundWorker_RunWorkerCompleted);
            moveBackgroundWorker.Dispose();

            (uiupdaterList[4] as MainWindow.OnFileMovingCompleted)();
        }

        #endregion






        private readonly List<string> _badSidPathList = new List<string>();
        public void removeBadSid(string badSidPath, ArrayList statusuiArrayList)
        {
            BadSidHandler badSidHandler = new BadSidHandler(badSidPath, statusuiArrayList, updateSidList);
            badSidHandler.HandleBadSid();
            _badSidPathList.Add(badSidPath);
        }

        #region Bad Sid Removing
        ///-----------------------------------------------------------------
        ///   Namespace:      PhotoBucket
        ///   Class:          BadSidHandler
        ///   Description:    Add Current user to access files, Remove unknow user SID from file security. search subdirectories
        ///   Author:         Mostafizur Rahman, Dots-sof.com                    Date: 28 January, 2017
        ///   Notes:          Partial class to handle bad sids.
        ///   Revision History:
        ///   Name:           Date:        Description:
        ///-----------------------------------------------------------------
        private class BadSidHandler
        {
            private readonly string badSidDirectoryPath;
            BackgroundWorker badsidBackgroundWorker;
            private readonly string UserName = WindowsIdentity.GetCurrent().Name;
            List<DirectoryInfo> badSidDirectoryInfoList;

            private ArrayList uiupdaterArrayList;
            private SID_INFO sidInfo;
            private OnSidListUpdated delegation;
            public BadSidHandler(string badSidDirectory, ArrayList statusArrayList, OnSidListUpdated sidDelegation)
            {
                badSidDirectoryPath = badSidDirectory;
                Console.WriteLine(UserName);
                uiupdaterArrayList = statusArrayList;
                badSidDirectoryInfoList = new List<DirectoryInfo>() { };
                delegation = sidDelegation;
                initializeBackgroundWorker();
            }

            private void initializeBackgroundWorker()
            {
                badsidBackgroundWorker = new BackgroundWorker();
                badsidBackgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
                badsidBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
                badsidBackgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
                badsidBackgroundWorker.WorkerReportsProgress = true;

            }

            public void HandleBadSid()
            {
                ArrayList parameterList = new ArrayList();
                sidInfo = new SID_INFO();
                sidInfo.TOTAL_FILE_COUNT = 0;
                sidInfo.SID_ID_LIST = new List<string>();
                sidInfo.SID_ROOT_DIR = badSidDirectoryPath;
                parameterList.Add(badSidDirectoryInfoList);
                parameterList.Add(sidInfo);
                badsidBackgroundWorker.RunWorkerAsync(parameterList);
            }

            private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
            {

                (sender as BackgroundWorker).ReportProgress(0);
                ArrayList paramList = e.Argument as ArrayList;
                List<DirectoryInfo> dirInfoList = paramList[0] as List<DirectoryInfo>;
                dirInfoList = getBadSidDirectoryList(badSidDirectoryPath);
                (sender as BackgroundWorker).ReportProgress(1);
                processBadSid(dirInfoList);
                (sender as BackgroundWorker).ReportProgress(2);
                remvoeBadSids(dirInfoList, paramList[1] as SID_INFO);
            }
            private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            {
                (uiupdaterArrayList[1] as ProgressBar).IsIndeterminate = false;
                (uiupdaterArrayList[0] as Label).Content = "Successfully bad sids removed! ಠ‿ಠ";
                (uiupdaterArrayList[2] as Label).Content = String.Format("Unknown sid found in : {0} files, Unknown id count : {1} [for more info see statistics]",
                    sidInfo.TOTAL_FILE_COUNT, sidInfo.SID_ID_LIST.Count);
                delegation(sidInfo);
            }

            private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
            {
                if (e.ProgressPercentage == 0)
                {
                    (uiupdaterArrayList[0] as Label).Content = "Getting subdirectories...";
                    (uiupdaterArrayList[1] as ProgressBar).IsIndeterminate = true;
                }
                else if (e.ProgressPercentage == 1)
                {
                    (uiupdaterArrayList[0] as Label).Content = "Granting access for administrative user account to the files...";
                }
                else if (e.ProgressPercentage == 2)
                {
                    (uiupdaterArrayList[0] as Label).Content = "Removing bad sid from image files and cleaning...";
                }
                else if (e.ProgressPercentage == 3)
                {

                }
            }

            public void processBadSid(List<DirectoryInfo> dirInfoList)
            {

                if (dirInfoList.Count > 0)
                {
                    foreach (DirectoryInfo directoryInfo in dirInfoList)
                    {
                        try
                        {
                            FileInfo[] fileInfoArray = directoryInfo.GetFiles();
                            foreach (FileInfo fileInfo in fileInfoArray)
                            {
                                setAccessRule(fileInfo);
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }

            public void remvoeBadSids(List<DirectoryInfo> dirInfoList, SID_INFO sidInfo)
            {
                if (dirInfoList.Count > 0)
                {
                    foreach (DirectoryInfo directoryInfo in dirInfoList)
                    {
                        try
                        {
                            FileInfo[] fileInfoArray = directoryInfo.GetFiles();
                            foreach (FileInfo fileInfo in fileInfoArray)
                            {
                                removeAccessRule(fileInfo, sidInfo);
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }

            public static List<DirectoryInfo> getBadSidDirectoryList(string filePath)
            {
                DirectoryInfo badSidDirectoryInfo = new DirectoryInfo(filePath);
                List<DirectoryInfo> directoryInfoList = new List<DirectoryInfo>();
                List<DirectoryInfo> dirInfoStack = new List<DirectoryInfo>();
                directoryInfoList.Add(badSidDirectoryInfo);
                dirInfoStack.Add(badSidDirectoryInfo);
                while (dirInfoStack.Count > 0)
                {
                    DirectoryInfo directoryInfo = dirInfoStack.ElementAt(0);

                    dirInfoStack.RemoveAt(0);
                    Console.WriteLine(directoryInfo.FullName);
                    try
                    {

                        DirectoryInfo[] directoryArray = directoryInfo.GetDirectories();
                        dirInfoStack.AddRange(directoryArray);
                        foreach (DirectoryInfo dirInfo in directoryArray)
                        {
                            directoryInfoList.Add(dirInfo);
                        }
                    }
                    catch
                    {

                    }
                }
                return directoryInfoList;
            }

            private void setAccessRule(FileInfo fileInfo)
            {
                try
                {
                    FileSecurity fileSecurity = File.GetAccessControl(fileInfo.FullName);
                    fileSecurity.AddAccessRule(new FileSystemAccessRule(UserName,
                        FileSystemRights.FullControl, AccessControlType.Allow));
                    File.SetAccessControl(fileInfo.FullName, fileSecurity);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Access Denied To Set Permission");
                }
                finally
                {

                }
            }
            private void removeAccessRule(FileInfo fileInfo, SID_INFO sidInfo)
            {

                FileSecurity fileSecurity = File.GetAccessControl(fileInfo.FullName);
                var rules = fileSecurity.GetAccessRules(true, false, typeof(SecurityIdentifier));
                foreach (var rule in rules.OfType<FileSystemAccessRule>())
                {
                    try
                    {
                        var account = rule.IdentityReference.Translate(typeof(NTAccount));
                    }
                    catch (IdentityNotMappedException)
                    {
                        try
                        {
                            if (fileSecurity.RemoveAccessRule(new FileSystemAccessRule(rule.IdentityReference,
                                FileSystemRights.FullControl, AccessControlType.Allow)))
                            {
                                fileInfo.SetAccessControl(fileSecurity);
                                sidInfo.TOTAL_FILE_COUNT++;
                                if (!sidInfo.SID_ID_LIST.Contains(rule.IdentityReference.ToString()))
                                {
                                    sidInfo.SID_ID_LIST.Add(rule.IdentityReference.ToString());
                                    Console.WriteLine("removed " + rule.IdentityReference.ToString());
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Access Denied To Set Permission");
                        }
                    }
                }



            }
            ~BadSidHandler()
            {
                badsidBackgroundWorker.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
                badsidBackgroundWorker.ProgressChanged -= new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
                badsidBackgroundWorker.DoWork -= new DoWorkEventHandler(backgroundWorker_DoWork);
                Console.WriteLine("re@@@move background worker%%%%%%%%%%%##################");
                int ocunt = uiupdaterArrayList.Count;
                for (int i = 0; i < ocunt; i++)
                {
                    var obj = uiupdaterArrayList[0];
                    obj = null;
                }
            }
        }
        #endregion

        private class SID_INFO
        {
            public string SID_ROOT_DIR { get; set; }
            public List<string> SID_ID_LIST { get; set; }
            public long TOTAL_FILE_COUNT { get; set; }
        }
    }
}

