﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace PhotoBucket
{
    class BucketConstant
    {
        [Flags]
        public enum MOVE_TYPE
        {

            MoveFolderHierarchy = 0x01,

            MoveFolderDateTime = 0x02,

            MoveFolderComments = 0x03
        }

        public static ArrayList MONTH_LIST = new ArrayList() {  "January", "February", "March",
                                                         "April", "May", "June",
                                                         "July", "August", "September",
                                                         "Octobar", "November", "December"};
        public readonly static string RESOURCE_IS_BEING_USED = 
            "The process cannot access the file because it is being used by another process.";
    }
}
