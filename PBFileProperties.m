//
//  PBImageProperties.m
//  SameImageCutter
//
//  Created by Mostafizur Rahman on 10/17/16.
//  Copyright © 2016 Mostafizur Rahman. All rights reserved.
//

#import "PBFileProperties.h"
#import "PBUserInterfaceProvider.h"



@implementation PBFileProperties


-(instancetype)initWithUrl:(NSURL *)imageUrl thumb:(NSURL *)thumbUrl{
    self = [super init];
    
    if(self){
        [self getImagePropertiesFrom:imageUrl thumb:(NSURL *)thumbUrl];
    }
    
    return self;
}

-(void)getImagePropertiesFrom:(NSURL *)imageFileUrl thumb:(NSURL *)thumbUrl{
    
    self.fileType = imageFileUrl.pathExtension;
    self.fileName = imageFileUrl.lastPathComponent;
    self.fileUrl = imageFileUrl;
    self.thumbImageURL = thumbUrl;
    NSError *error = nil;
    NSDictionary *attribs = [[NSFileManager defaultManager] attributesOfItemAtPath:self.fileUrl.path error:&error];
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((__bridge CFURLRef)imageFileUrl, NULL);
    if (imageSource == NULL) {
        return;
    }
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:NO],
                             (NSString *)kCGImageSourceShouldCache, nil];
    
    CFDictionaryRef optionDicRef = (__bridge CFDictionaryRef)(options);
    CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, optionDicRef);
    if (imageProperties == NULL) {
        CFRelease(imageSource);
        return ;
    }
    CFDictionaryRef dicref = CGImageSourceCopyProperties(imageSource, optionDicRef);
    id dictValue = (__bridge id)CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
    self.imagePixelHeight = [dictValue longValue];
    dictValue = (__bridge id)CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);
    self.imagePixelWidth = [dictValue longValue];
    dictValue = (__bridge id)CFDictionaryGetValue(dicref, kCGImagePropertyFileSize);
    self.fileDataLength = [dictValue longValue];
    
    dictValue = (__bridge id)CFDictionaryGetValue(imageProperties, kCGImagePropertyTIFFDictionary);
    
    NSDictionary *tiff = (NSDictionary *)dictValue;
    if (tiff) {
        NSString *cameraModel =  [tiff objectForKey:(id)kCGImagePropertyTIFFModel];
        NSString *lenseModel = [tiff objectForKey:(id)kCGImagePropertyTIFFMake];
        if(lenseModel) {
            self.imageCameraModel = [NSString stringWithFormat:FORMAT_CAMMODEL, cameraModel, lenseModel];
        } else {
            self.imageCameraModel = cameraModel;
        }
        self.softwareName = [tiff objectForKey:(id)kCGImagePropertyTIFFSoftware];
        NSString *fileCreationDate = [tiff objectForKey:(id)kCGImagePropertyTIFFDateTime];
        if(fileCreationDate){
            
            NSArray *dateTimeArray = [fileCreationDate componentsSeparatedByString:FORMAT_UTFSPACE];
            NSArray *dateArray = [[dateTimeArray objectAtIndex:0] componentsSeparatedByString:FORMAT_COLON];
            NSArray *timeArray = [[dateTimeArray objectAtIndex:1] componentsSeparatedByString:FORMAT_COLON];
            self.fileCreationDate = [[NSDateComponents alloc] init];
            self.fileCreationDate.year = [[dateArray objectAtIndex:0] integerValue];
            self.fileCreationDate.month = [[dateArray objectAtIndex:1] integerValue];
            self.fileCreationDate.day = [[dateArray objectAtIndex:2] integerValue];
            self.fileCreationDate.hour = [[timeArray objectAtIndex:0] integerValue];
            self.fileCreationDate.minute = [[timeArray objectAtIndex:1] integerValue];
            self.fileCreationDate.second = [[timeArray objectAtIndex:2] integerValue];
        }
    }
    dictValue = (__bridge id)CFDictionaryGetValue(imageProperties, kCGImagePropertyGPSDictionary);
    NSDictionary *gps = (NSDictionary *)dictValue;
    if (gps) {
        NSString *latitudeString = [gps objectForKey:(id)kCGImagePropertyGPSLatitude];
        NSString *latitudeRef = [gps objectForKey:(id)kCGImagePropertyGPSLatitudeRef];
        NSString *longitudeString = [gps objectForKey:(id)kCGImagePropertyGPSLongitude];
        NSString *longitudeRef = [gps objectForKey:(id)kCGImagePropertyGPSLongitudeRef];
        self.imageGPSPosition = [NSString stringWithFormat:FORMAT_GPS,
                                 longitudeString, longitudeRef, latitudeString, latitudeRef];
    }
    dictValue = (__bridge id)CFDictionaryGetValue(imageProperties, kCGImagePropertyExifDictionary);
    NSDictionary *exif = (NSDictionary *)dictValue;
    NSString *comment = EMPTY_STRING;
    if(exif){
        comment = [[exif objectForKey:(id)kCGImagePropertyExifUserComment] stringByTrimmingCharactersInSet:[NSCharacterSet controlCharacterSet]];
        NSString *cameraModel = [exif objectForKey:(id)kCGImagePropertyExifLensModel];
        if(!self.imageCameraModel){
            if(cameraModel){
                NSString *lenseModel = [exif objectForKey:(id)kCGImagePropertyExifLensMake];
                if(lenseModel){
                    self.imageCameraModel = [NSString stringWithFormat:FORMAT_CAMMODEL, cameraModel, lenseModel];
                } else {
                    self.imageCameraModel = cameraModel;
                }
            } else {
                NSString *lenseModel = [exif objectForKey:(id)kCGImagePropertyExifLensMake];
                if(lenseModel){
                    self.imageCameraModel = lenseModel;
                } else {
                    self.imageCameraModel = UNKNOWN_MODEL;
                }
            }
        }
    }
    self.imageHashTag = [[comment stringByReplacingOccurrencesOfString:FORMAT_UTFSPACE withString:EMPTY_STRING]  isEqualToString:EMPTY_STRING] ||
                        !comment ? [self getComment:attribs[KEY_EXTATTRIBUTE]] : NOCOMMENT;
    [self setThumbnailImageFrom:imageSource];
    self.fileLastModified = attribs[KEY_DATEMODIFY];
    self.fileId = attribs[KEY_FILENUMBER];
    if(!self.fileCreationDate){
        NSDate *fileCreationDate =  attribs[KEY_DATEMODIFY];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        self.fileCreationDate = [calendar components:DATE_COMPONENTS fromDate:fileCreationDate];
    }
    
    CFRelease(dicref);
    CFRelease(imageProperties);
    CFRelease(imageSource);
}

-(NSString *)getComment:(NSDictionary *)commentDictionary{
    
    NSData *data = [commentDictionary objectForKey:KEY_FINDERCOMMENT];
    NSString *comments = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if([comments isEqualToString:EMPTY_STRING] || [comments length] < 9) return NOCOMMENT;
    return [[comments stringByTrimmingCharactersInSet:[NSCharacterSet controlCharacterSet]] substringFromIndex:9];
}

-(void)setThumbnailImageFrom:(CGImageSourceRef)imageSource {
    
    if(imageSource != NULL) {
        NSDictionary *thumbnailOptions = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [NSNumber numberWithBool:YES],
                                          (NSString *)kCGImageSourceCreateThumbnailFromImageIfAbsent,
                                          [NSNumber numberWithInt:MAX_THUMB_DIMENSION],
                                          (NSString *)kCGImageSourceThumbnailMaxPixelSize, nil];
        
        CGImageRef thumbnailRef = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, (CFDictionaryRef)thumbnailOptions );
        if (thumbnailRef != NULL) {
            
            NSImage *thumbImage = [[NSImage alloc] initWithCGImage:thumbnailRef size:CGSizeZero];
            NSURL *thumbImageURL = [self.thumbImageURL URLByAppendingPathComponent:self.fileName];
            while([[NSFileManager defaultManager] fileExistsAtPath:thumbImageURL.path]){
                NSString* extension = thumbImageURL.pathExtension;
                thumbImageURL = [thumbImageURL URLByDeletingPathExtension];
                NSString *fileName = thumbImageURL.lastPathComponent;
                thumbImageURL = [thumbImageURL URLByDeletingLastPathComponent];
                fileName = [NSString stringWithFormat:FORMAT_DUPFILENAME,fileName,
                            [PBUserInterfaceProvider getRandomStringOfLen:RANDOMSTRING_LENGTH], extension];
                thumbImageURL = [thumbImageURL URLByAppendingPathComponent:fileName];
            }
            if([self.fileType.lowercaseString isEqualToString:[IMAGE_TYPE objectAtIndex:0]]) {
                if([thumbImage saveImageAsPng:thumbImageURL.path withImageRef:thumbnailRef]){
                    self.thumbImageURL = thumbImageURL;
                    thumbImage = nil;
                }
                else{
                    self.thumbImageURL = nil;
                }
            }
            else{
                if([thumbImage saveAsJpegWithName:thumbImageURL.path]){
                    self.thumbImageURL = thumbImageURL;
                }
                else{
                    self.thumbImageURL = nil;
                }
            }
            
        }
        CGImageRelease(thumbnailRef);
    }
}



/*NSString *file = @"…"; // path to some file
 CFStringRef fileExtension = (CFStringRef) [file pathExtension];
 CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);
 
 if (UTTypeConformsTo(fileUTI, kUTTypeImage)) NSLog(@"It's an image");
 else if (UTTypeConformsTo(fileUTI, kUTTypeMovie)) NSLog(@"It's a movie");
 else if (UTTypeConformsTo(fileUTI, kUTTypeText)) NSLog(@"It's text");
 
 CFRelease(fileUTI);
 
 */


@end
